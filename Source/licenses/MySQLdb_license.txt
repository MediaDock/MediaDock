MySQLdb is `Free Software`_.

.. _MySQL: http://www.mysql.com/
.. _`Free Software`: http://www.gnu.org/
.. [PEP-0249] http://www.python.org/peps/pep-0249.html

    Author: Andy Dustman
    Author_email: farcepest@gmail.com
    Description: Python interface to MySQL
    Name: MySQL-python
    Url: http://sourceforge.net/projects/mysql-python
    Version: 1.2.4b4
