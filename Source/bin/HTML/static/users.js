var user_modal_counter = 0;

function getUsersData(){
    data = getJsonData({'function': 'getUsersData'}, true);
    var elem_table_users_body = document.getElementById("table-users-body");
    if (data.result.check){
        html = ''
        for (var user in data.result.data) {
            if (data.result.data.hasOwnProperty(user)) {
                html += '<tr>';
                html += '<td>' + user + '</td>';
                html += '<td>' + data.result.data[user] + '</td>';
                html += '<td> <div class="btn-group btn-group">';
                html += '<button type="button" class="btn btn-default" onclick="changePasswordModal(' + "'" + user + "'" + ')"> Passwort ändern </button>';
                if (data.result.data[user] != ''){
                    html += '<button type="button" class="btn btn-default" onclick="changeRightsModal(' + "'" + user + "'" + ')"> Rechte ändern </button>'
                }
                html += '<button type="button" class="btn btn-default" onclick="deleteUserModal(' + "'" + user + "'" + ')"> Benutzer löschen </button>'
                html += '</div></tr>'
            }
        }
        elem_table_users_body.innerHTML = html;
    }
    else{
        elem_table_users_body.innerHTML = '';
    }
}

function changePasswordModal(user_name){

    if (user_name == undefined){
        data = getJsonData({'function': 'checkSession'}, true);
        user_name = data.info;
    }

    if (user_name == ''){
        showNewModal("Kein Benutzer ist eingeloggt.", 'Info', 1);
        return;
    }

    data = getJsonData({'function': 'checkSession'}, true);
    admin_user_name = data.info;

    data = getJsonData({'function': 'getUsersData'}, true);
    admin_user_rights = data.result.data[admin_user_name];

    if (user_name != admin_user_name && (admin_user_rights == undefined || admin_user_rights == '' || admin_user_rights.slice(0, 1) != '1')){
        showNewModal('Sie verfügen nicht über das notwendige Recht', 'Info', 1);
        return;
    }

    var user_modal_text = '<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-dialog modal-lg"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><h4 class="modal-title" id="myModalLabel">'
                     + 'Passwort ändern für ' + user_name + '</h4></div><div class="modal-body"><div class="form-group"><label for="inputName">Neues Passwort</label><input type="password" class="form-control" id="inputPassword1'
                     + user_modal_counter + 'user" placeholder="Passwort"></div><div class="form-group"><label for="exampleInputPassword1">Passwort wiederholen</label><input type="password" class="form-control" id="inputPassword2'
                     + user_modal_counter + 'user" placeholder="Passwort"></div>'
                     + '<div align="center"><div id="keyboard'
                     + user_modal_counter + 'user"></div></div>'
                     + '<script type="text/javascript">'
                     + 'var kb = new keyboard("keyboard'
                     + user_modal_counter + 'user", { width: 40, height: 40, keymap: ' + "'" + 'ge' + "'" + ' });'
                     + 'kb.add_keymap(' + "'" + 'ge' + "'" + ', maps[' + "'" + 'ge' + "'" + ']); '
                     + 'kb.show();</script>'
                     + '</div><div class="modal-footer"><button type="button" class="btn btn-default" '
                     + 'data-dismiss="modal" onclick="changePassword(' + "'" + user_name + "', '"
                     + user_modal_counter + "user'" + ')">Abschicken</button><button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button></div></div></div></div>';
    user_modal_counter++;
    $(user_modal_text).modal();
    user_modal_text = '';
}

function changePassword(user_name, id_suffix){
    data = getJsonData({ 'function': 'changePassword',
                        'user_name': user_name,
                        'password1': document.getElementById('inputPassword1' + id_suffix).value,
                        'password2': document.getElementById('inputPassword2' + id_suffix).value}
        , true);

    if(! data.result.check){
        showNewModal(data.result.message, 'Info', 1);
    }
}

function addUserModal(){

    data = getJsonData({'function': 'checkSession'}, true);
    admin_user_name = data.info;

    data = getJsonData({'function': 'getUsersData'}, true);
    admin_user_rights = data.result.data[admin_user_name];

    if (admin_user_rights == undefined || admin_user_rights == '' || admin_user_rights.slice(0, 1) != '1'){
        showNewModal('Sie verfügen nicht über das notwendige Recht', 'Info', 1);
        return;
    }

    var add_user_modal_text = '<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-dialog modal-lg"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><h4 class="modal-title" id="myModalLabel">'
                     + 'Benutzer hinzufügen</h4></div>'
                     + '<div class="modal-body"><div class="form-group"><label for="inputName">Name</label><input type="text" class="form-control" id="name'
                     + user_modal_counter + 'adduser" placeholder="Name"></div><div class="form-group"><label for="exampleInputPassword1">Passwort</label><input type="password" class="form-control" id="inputPassword1'
                     + user_modal_counter + 'adduser" placeholder="Passwort"></div><div class="form-group"><label for="exampleInputPassword1">Passwort wiederholen</label><input type="password" class="form-control" id="inputPassword2'
                     + user_modal_counter + 'adduser" placeholder="Passwort">'
                     + '</div><div class="form-group"><label for="">Rechte</label>'
                     + '<div class="checkbox">'
                     + '    <label><input type="checkbox" value="" id="rootright' + user_modal_counter + 'adduser">Root</label>'
                     + '</div>'
                     + '<div class="checkbox">'
                     + '    <label><input type="checkbox" value="" id="viewright' + user_modal_counter + 'adduser">Namen aller Benutzer sehen</label>'
                     + '</div>'
                     + '<div class="checkbox">'
                     + '    <label><input type="checkbox" value="" id="unlockright' + user_modal_counter + 'adduser">Bildschirm sperren / entsperren</label>'
                     + '</div>'
                     + '<div class="checkbox">'
                     + '    <label><input type="checkbox" value="" id="quitright' + user_modal_counter + 'adduser">Programm beenden</label>'
                     + '</div>'
                     + '<div class="checkbox">'
                     + '    <label><input type="checkbox" value="" id="shutdownright' + user_modal_counter + 'adduser">Rechner herunterfahren</label>'
                     + '</div>'
                     + '<div class="checkbox">'
                     + '    <label><input type="checkbox" value="" id="shownormalright' + user_modal_counter + 'adduser">Programm im normalen Fenster anzeigen lassen</label>'
                     + '</div>'
                     + '<div class="checkbox">'
                     + '    <label><input type="checkbox" value="" id="manipulatefsmright' + user_modal_counter + 'adduser">FSM manipulieren</label>'
                     + '</div>'
                     + '<div class="checkbox">'
                     + '    <label><input type="checkbox" value="" id="statisticsright' + user_modal_counter + 'adduser">Statistik sehen</label>'
                     + '</div>'
                     + '<div class="checkbox">'
                     + '    <label><input type="checkbox" value="" id="settingsright' + user_modal_counter + 'adduser">Einstellungen änderen</label>'
                     + '</div>'
                     + '</div>'
                     + '<div align="center"><div id="keyboard'
                     + user_modal_counter + 'adduser"></div></div>'
                     + '<script type="text/javascript">'
                     + 'var kb = new keyboard("keyboard'
                     + user_modal_counter + 'adduser", { width: 40, height: 40, keymap: ' + "'" + 'ge' + "'" + ' });'
                     + 'kb.add_keymap(' + "'" + 'ge' + "'" + ', maps[' + "'" + 'ge' + "'" + ']); '
                     + 'kb.show();</script>'
                     + '</div><div class="modal-footer"><button type="button" class="btn btn-default" '
                     + 'data-dismiss="modal" onclick="addUser(' + "'"
                     + user_modal_counter + "adduser'" + ')">Abschicken</button><button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button></div></div></div></div>';
    user_modal_counter++;
    $(add_user_modal_text).modal();
    add_user_modal_text = '';
}

function addUser(id_suffix){
    user_name = document.getElementById('name' + id_suffix).value;
    password1 = document.getElementById('inputPassword1' + id_suffix).value;
    password2 = document.getElementById('inputPassword2' + id_suffix).value;
    rights = '';

    rights = rights.concat(document.getElementById('rootright' + id_suffix).checked ? '1' : '0');
    rights = rights.concat(document.getElementById('viewright' + id_suffix).checked ? '1' : '0');
    rights = rights.concat(document.getElementById('unlockright' + id_suffix).checked ? '1' : '0');
    rights = rights.concat(document.getElementById('quitright' + id_suffix).checked ? '1' : '0');
    rights = rights.concat(document.getElementById('shutdownright' + id_suffix).checked ? '1' : '0');
    rights = rights.concat(document.getElementById('shownormalright' + id_suffix).checked ? '1' : '0');
    rights = rights.concat('0');
    rights = rights.concat(document.getElementById('manipulatefsmright' + id_suffix).checked ? '1' : '0');
    rights = rights.concat(document.getElementById('statisticsright' + id_suffix).checked ? '1' : '0');
    rights = rights.concat(document.getElementById('settingsright' + id_suffix).checked ? '1' : '0');
    rights = rights.concat('0');
    rights = rights.concat('0');
    rights = rights.concat('0');
    rights = rights.concat('0');
    rights = rights.concat('0');
    rights = rights.concat('0');

    data = getJsonData({ 'function': 'addUser',
                        'user_name': user_name,
                        'password1': password1,
                        'password2': password2,
                        'rights': rights}
        , true);

    if(! data.result.check){
        showNewModal(data.result.message, 'Info', 1);
    }
    getUsersData();
}

function deleteUserModal(user_name){

    data = getJsonData({'function': 'checkSession'}, true);
    admin_user_name = data.info;

    data = getJsonData({'function': 'getUsersData'}, true);
    admin_user_rights = data.result.data[admin_user_name];

    if (admin_user_rights == undefined || admin_user_rights == '' || admin_user_rights.slice(0, 1) != '1'){
        showNewModal('Sie verfügen nicht über das notwendige Recht', 'Info', 1);
        return;
    }

    var delete_user_modal_text = '<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-dialog modal-lg"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><h4 class="modal-title" id="myModalLabel">'
                     + 'Benutzer löschen</h4></div>'
                     + '<div class="modal-body">'
                     + 'Soll der Benutzer wirklich gelöscht werden?</div>'
                     + '<div class="modal-footer"><button type="button" class="btn btn-default" '
                     + 'data-dismiss="modal" onclick="deleteUser(' + "'"
                     + user_name + "'" + ')">Ja</button><button type="button" class="btn btn-default" data-dismiss="modal">Nein</button></div></div></div></div>';
    $(delete_user_modal_text).modal();
    delete_user_modal_text = '';
}

function deleteUser(user_name){

    data = getJsonData({ 'function': 'deleteUser',
                        'user_name': user_name}
        , true);

    if(! data.result.check){
        showNewModal(data.result.message, 'Info', 1);
    }
    getUsersData();
}

function changeRightsModal(user_name){

    data = getJsonData({'function': 'checkSession'}, true);
    admin_user_name = data.info;

    data = getJsonData({'function': 'getUsersData'}, true);
    admin_user_rights = data.result.data[admin_user_name];

    if (admin_user_rights == undefined || admin_user_rights == '' || admin_user_rights.slice(0, 1) != '1'){
        showNewModal('Sie verfügen nicht über das notwendige Recht', 'Info', 1);
        return;
    }

    old_rights = data.result.data[user_name];

    rootright = old_rights.slice(0, 1) == '1' ? 'checked' : '';
    viewright = old_rights.slice(1, 2) == '1' ? 'checked' : '';
    unlockright = old_rights.slice(2, 3) == '1' ? 'checked' : '';
    quitright = old_rights.slice(3, 4) == '1' ? 'checked' : '';
    shutdownright = old_rights.slice(4, 5) == '1' ? 'checked' : '';
    shownormalright = old_rights.slice(5, 6) == '1' ? 'checked' : '';
    manipulatefsmright = old_rights.slice(7, 8) == '1' ? 'checked' : '';
    statisticsright = old_rights.slice(8, 9) == '1' ? 'checked' : '';
    settingsright = old_rights.slice(9, 10) == '1' ? 'checked' : '';

    var change_rights_modal_text = '<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-dialog modal-lg"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><h4 class="modal-title" id="myModalLabel">'
                     + 'Rechte ändern für ' + user_name + '</h4></div>'
                     + '<div class="modal-body"><div class="form-group"><label for="">Rechte</label>'
                     + '<div class="checkbox">'
                     + '    <label><input type="checkbox" value="" id="rootright' + user_modal_counter + 'changerights" ' + rootright + '>Root</label>'
                     + '</div>'
                     + '<div class="checkbox">'
                     + '    <label><input type="checkbox" value="" id="viewright' + user_modal_counter + 'changerights" ' + viewright + '>Namen aller Benutzer sehen</label>'
                     + '</div>'
                     + '<div class="checkbox">'
                     + '    <label><input type="checkbox" value="" id="unlockright' + user_modal_counter + 'changerights"' + unlockright + '>Bildschirm sperren / entsperren</label>'
                     + '</div>'
                     + '<div class="checkbox">'
                     + '    <label><input type="checkbox" value="" id="quitright' + user_modal_counter + 'changerights"' + quitright + '>Programm beenden</label>'
                     + '</div>'
                     + '<div class="checkbox">'
                     + '    <label><input type="checkbox" value="" id="shutdownright' + user_modal_counter + 'changerights"' + shutdownright + '>Rechner herunterfahren</label>'
                     + '</div>'
                     + '<div class="checkbox">'
                     + '    <label><input type="checkbox" value="" id="shownormalright' + user_modal_counter + 'changerights"' + shownormalright + '>Programm im normalen Fenster anzeigen lassen</label>'
                     + '</div>'
                     + '<div class="checkbox">'
                     + '    <label><input type="checkbox" value="" id="manipulatefsmright' + user_modal_counter + 'changerights"' + manipulatefsmright + '>FSM manipulieren</label>'
                     + '</div>'
                     + '<div class="checkbox">'
                     + '    <label><input type="checkbox" value="" id="statisticsright' + user_modal_counter + 'changerights"' + statisticsright + '>Statistik sehen</label>'
                     + '</div>'
                     + '<div class="checkbox">'
                     + '    <label><input type="checkbox" value="" id="settingsright' + user_modal_counter + 'changerights"' + settingsright + '>Einstellungen änderen</label>'
                     + '</div>'
                     + '</div>'
                     + '</div><div class="modal-footer"><button type="button" class="btn btn-default" '
                     + 'data-dismiss="modal" onclick="changeRights(' + "'" + user_name + "', '"
                     + user_modal_counter + "changerights'" + ')">Abschicken</button><button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button></div></div></div></div>';
    user_modal_counter++;
    $(change_rights_modal_text).modal();
    change_rights_modal_text = '';

}

function changeRights(user_name, id_suffix){

    rights = '';

    rights = rights.concat(document.getElementById('rootright' + id_suffix).checked ? '1' : '0');
    rights = rights.concat(document.getElementById('viewright' + id_suffix).checked ? '1' : '0');
    rights = rights.concat(document.getElementById('unlockright' + id_suffix).checked ? '1' : '0');
    rights = rights.concat(document.getElementById('quitright' + id_suffix).checked ? '1' : '0');
    rights = rights.concat(document.getElementById('shutdownright' + id_suffix).checked ? '1' : '0');
    rights = rights.concat(document.getElementById('shownormalright' + id_suffix).checked ? '1' : '0');
    rights = rights.concat('0');
    rights = rights.concat(document.getElementById('manipulatefsmright' + id_suffix).checked ? '1' : '0');
    rights = rights.concat(document.getElementById('statisticsright' + id_suffix).checked ? '1' : '0');
    rights = rights.concat(document.getElementById('settingsright' + id_suffix).checked ? '1' : '0');
    rights = rights.concat('0');
    rights = rights.concat('0');
    rights = rights.concat('0');
    rights = rights.concat('0');
    rights = rights.concat('0');
    rights = rights.concat('0');

    data = getJsonData({ 'function': 'setUserRights',
                        'user_name': user_name,
                        'rights': rights}
        , true);

    if(! data.result.check){
        showNewModal(data.result.message, 'Info', 1);
    }

    getUsersData();

}

getUsersData();
