# -*- coding: utf-8 -*-

import calendar
import datetime
import os
import threading

from DriveHandling import Drive
import DefaultSettings
import EncrConst
import MDConst
import OsInterface as Osi
from CDOrders import CDOrderHandler

MONTH_DICT_GE = {1: u'Januar',
                 2: u'Februar',
                 3: u'März',
                 4: u'April',
                 5: u'Mai',
                 6: u'Juni',
                 7: u'Juli',
                 8: u'August',
                 9: u'September',
                 10: u'Oktober',
                 11: u'November',
                 12: u'Dezember'
                 }

DAY_DICT_GE = {0: u'Montag',
               1: u'Dienstag',
               2: u'Mittwoch',
               3: u'Donnerstag',
               4: u'Freitag',
               5: u'Samstag',
               6: u'Sonntag'
               }


class ServerInterface(object):
    def __init__(self, engine):
        self.engine = engine
        self.numberOfFinishErrors = 0
        self.numberOfRequests = 0

    def incrNumberOfFinishErrors(self):
        self.numberOfFinishErrors += 1
        self.engine.master.i.write_info(u'NumberOfServerErrors', self.numberOfFinishErrors)

    def incrNumberOfRequests(self):
        self.numberOfRequests += 1
        self.engine.master.i.write_info(u'NumberOfServerRequests', self.numberOfRequests)

    def getDriveMessagesSidebar(self):
        html = ''
        for i in self.engine.dh.drives:
            html += u'''<div class="volume">
                    Laufwerk %s<br>

                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="%i" aria-valuemin="0"
                        aria-valuemax="%i" style="width: %i%%;">
                        </div>
                    </div>
                    %s frei von %s
                    </div>''' % (i, i.totalSpace - i.freeSpace, i.totalSpace,
                                 ((i.totalSpace - i.freeSpace) * 100) / i.totalSpace,
                                 self.engine.fh.getSpaceStr(i.freeSpace),
                                 self.engine.fh.getSpaceStr(i.totalSpace))
        if html == u'':
            html = u'<div class="volume">Kein Laufwerk angeschlossen</div>'
        return html

    def getMessageMessagesSidebar(self):
        html = ''
        for i in (3, 2, 1, 0):
            for j in self.engine.mh.getSortedMessages(1, i):  # todo section
                html += u'<div class="message%i" id="message%i">%s</div>' % (i, i, j.message)
        if html == u'':
            return u'<div class="message_null" id="message_null">Keine Meldung</div>'
        return html

    def getJobMessagesSidebar(self):
        html = ''
        if len(self.engine.jh.jobs) == 0:
            html = u'<div class="no_job" id="no_job">Kein Auftrag</div>'
        else:
            for i in self.engine.jh.jobs:
                html += u'<div class="job" id="job">'
                html += u'Ziel: %s' % i.drive.name
                html += u'<ul>'
                for j in i.recordList:
                    html += u'<li>%s %i.%02i.%04i</li>' % (j.event, j.day, j.month, j.year)
                html += u'</ul>'
                progress = i.getProgress()
                left_time = i.getLeftTime()
                '''
                if progress[1] > 0:
                    progress_percent = ((progress[0] + 1) * 100)/progress[1]
                else:
                    progress_percent = 0
                '''
                progress_percent = i.getProgressSizePercent()
                html += r'<div class="progress"><div class="progress-bar progress-bar-success progress-bar-striped" ' \
                        r'style="width:%i%%;min-width: 2em;" role="progressbar"> %i%% </div></div>' % \
                        (progress_percent, progress_percent)
                html += u'Datei %i von %i' % ((progress[0] + 1), progress[1])
                html += u'<br>Vorraussichtliche Dauer: %s' % left_time
                html += u'</div>'
        return html

    def getModalMessages(self):
        return self.engine.mh.getModalMessages()

    def getDrives(self):
        html = ''
        for i in self.engine.dh.drives:
            html += u'''<div class="drive" id="drive">
            Laufwerk %s <br>
            <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="%i" aria-valuemin="0" aria-valuemax="%i"
                style="width: %i%%;">
                </div>
            </div>
            %s frei von %s<br>
            <div class="btn-group">
            <button type="button" class="btn btn-default" onclick="ask_eject_drive('%i', '%s')">Laufwerk auswerfen</button>
            </div>
            </div><br>''' % (
                i, i.totalSpace - i.freeSpace, i.totalSpace, ((i.totalSpace - i.freeSpace) * 100) / i.totalSpace,
                self.engine.fh.getSpaceStr(i.freeSpace), self.engine.fh.getSpaceStr(i.totalSpace), i.id, unicode(i))
        if html == u'':
            html = u'<div class="nodrive" id="nodrive">Kein Laufwerk angeschlossen</div>'
        return html

    def ejectDrive(self, drive_id):
        eject_thread = threading.Thread(target=self.engine.dh.ejectDrive, args=(drive_id,),
                                        name=u'EjectThreadDrive' + unicode(drive_id))
        eject_thread.start()

    def getDriveMessagesDrives(self):
        html = ''
        for i in (3, 2, 1, 0):
            for j in self.engine.mh.getSortedMessages(1, i):  # todo asd
                html += u'<div class="message%i" id="message%i">%s</div>' % (i, i, j.message)
        if html == '':
            return html
        else:
            return html + u'<br>'

    def getMedia(self, path):
        return os.path.join(self.engine.fh.rootDir, path)

    def explorerGetDir(self, par_path):
        # path = self.master.fh.decodeURL(par_path)
        path = par_path
        if (path.startswith(u'Media') or path.startswith(u'/Media')) and self.engine.fh.rootDir is None:
            return u'Kein Aufnahmenverzeichnis angegeben.', u'Media', ''

        legal_paths = list()
        if not (self.engine.fh.rootDir is None):
            legal_paths.append(self.engine.fh.rootDir.replace('\\', '/'))
        for i in self.engine.dh.getMountPoints():
            legal_paths.append(i.replace('\\', '/'))

        if path.startswith(u'/Media'):
            path = self.engine.fh.rootDir + path[6:]
        elif path.startswith(u'Media'):
            path = self.engine.fh.rootDir + path[5:]
        if Osi.LINUX and (self.engine.fh.rootDir is None or
                              not path.startswith(self.engine.fh.rootDir)) and not path.startswith(
            '/media'):  # todo doesnt work with rootDir is None
            path = '/media/' + os.popen('echo "$USER"').read().decode('utf-8').strip('\n') + '/' + path.lstrip('/')
        elif Osi.WINDOWS and len(path) == 2:
            path += '/'
        path = path.replace('//', '/')

        if Osi.WINDOWS:
            if path.startswith('/'):
                path = '/' + path  # das ist für Server-Ordner unter Windows

        is_in = False
        for i in legal_paths:
            if path.startswith(i):
                is_in = True
                break

        if not is_in:
            return u'Der Pfad existiert nicht (mehr)', 'Media', ''

        # make path above list
        if not path == self.engine.fh.rootDir:  # todo wozu wird dass hier eigentlich gebraucht
            path_above = path
        else:
            path_above = self.engine.fh.rootDir
        if not self.engine.fh.rootDir is None:
            if len(path_above) < len(self.engine.fh.rootDir) and self.engine.fh.rootDir.startswith(path_above):
                path_above = self.engine.fh.rootDir
                path = self.engine.fh.rootDir
            path_above = path_above.replace(self.engine.fh.rootDir, u'Media')
        if Osi.LINUX:
            for i in legal_paths:
                if path_above.startswith(i):
                    path_above = '/'.join(path_above.split('/')[3:])
                    break
        path_above_list = path_above.split('/')
        while '' in path_above_list:
            path_above_list.remove('')
        # print path_above_list

        # make html
        html = u''

        if os.path.isdir(path):
            for i in self.engine.fh.getExplorerDirContent(path):
                if self.engine.fh.isLegalDir(path + u'/' + i) and not i.startswith(u'$'):
                    html += u'''<p name="exp_item"
                    id="dir_%s" ondblclick="doubleClick(event)" onclick="togglebgcolor(event)"><img border="0"
                     class="exp_img" src="file:///%s/HTML/static/images/folder.png" alt="Ordner" height="36" width="41"
                     style="vertical-align:bottom"> %s </p>''' % (path + '/' + i, os.path.abspath('.'), i)
                elif self.engine.fh.isLegalExplorerFile(path + u'/' + i):  # todo dateiendungen mit dict / list
                    if i.endswith(u'.mp3') or i.endswith(u'.wav'):
                        html += u'''<p name="exp_item"
                        id="%s_%s" ondblclick="doubleClick(event)" onclick="togglebgcolor(event)">
                        <img class="exp_img" border="0" src="file:///%s/HTML/static/images/music.png" alt="Music"
                        height="36" width="37" style="vertical-align:bottom"> %s </p>''' % \
                                (i[-3:], path + u'/' + i, os.path.abspath(u'.'), i)
                    else:
                        html += u'''<p name="exp_item"
                        id="fil_%s" onclick="togglebgcolor(event)">
                        <img class="exp_img" "border="0" src="file:///%s/HTML/static/images/file.png" alt="Datei"
                        height="36" width="28" style="vertical-align:bottom"> %s </p>''' % \
                                (path + u'/' + i, os.path.abspath(u'.'), i)
            if html == u'':
                html = u'Der Pfad ist leer oder die Dateien können nicht dargestellt werden'
        else:
            html = u'Der Pfad existiert nicht (mehr)'

        # read record data
        record_data = ''
        if 'record_info.xml' in os.listdir(path):
            try:
                record_info = self.engine.rh.read_record_info(path)
            except:
                record_data = u'Datei "record_info.xml" ist nicht auslesbar.'
            else:
                record_data = u'<table  class="table table-striped"><thead><th>record_info.xml</th><th/></thead>'
                record_data += u'<tbody>'
                record_data += u'<tr><th>Version</th><td>%i</td></tr>' % record_info['version']
                record_data += u'<tr><th>Tag</th><td>%i</td></tr>' % record_info['day']
                record_data += u'<tr><th>Monat</th><td>%s</td></tr>' % MONTH_DICT_GE[record_info['month']]
                record_data += u'<tr><th>Jahr</th><td>%i</td></tr>' % record_info['year']
                record_data += u'<tr><th>Ort</th><td>%s</td></tr>' % record_info['church']
                record_data += u'<tr><th>Veranstaltung</th><td>%s</td></tr>' % record_info['event']
                record_data += u'<tr><th>Zusätzliche Informationen</th><td>%s</td></tr>' % record_info['info']
                if record_info['version']:
                    record_data += u'<tr><th>Reihenfolge</th><td>%s</td></tr>' % record_info['order']
                else:
                    record_data += u'<tr class="danger"><th>Reihenfolge</th><td>%s</td></tr>' % record_info['order']
                record_data += u'</tbody></table>'

        return html, path_above_list, record_data

    def getExplorerDropdown(self):
        html = u'''<li><p onclick="getContent('dir_Media')">Media</p></li>'''
        for i in self.engine.dh.drives:
            html += u'''\n<li><p onclick="getContent('dir_%s')">%s</p></li>''' % (i.mountpoint, i)
        return html.replace('\\', '/')

    def getCompoundData(self):
        '''
        Rückgabe:
            [id, Belegung in %, freeSpace, totalSpace, Status, Fortschritt in % , x-te Datei von, X wird kopiert}
        '''
        compounds = []
        for i in self.engine.dh.drives:
            jobs = self.engine.jh.getJobsForDrive(i.id)
            progress = 1
            total_files = 0
            progress_percent = 0
            left_time = u'---'
            if jobs:
                progress, total_files = jobs[0].getProgress()
                progress_percent = jobs[0].getProgressSizePercent()
                left_time = jobs[0].getLeftTime()
            # for j in jobs:
            #    temp = j.getProgress()
            #    progress += temp[0]
            #    total_files += temp[1]
            if progress > total_files:
                progress = total_files
            compounds.append({u'id': i.id, u'ussage': i.getUsage(),
                              u'freeSpace': self.engine.fh.getSpaceStr(i.freeSpace),
                              u'totalSpace': self.engine.fh.getSpaceStr(i.totalSpace), u'state': i.state,
                              u'progress': progress, u'progress_percent': progress_percent, u'total_files': total_files,
                              u'name': unicode(i), u'left_time': left_time})
        return compounds

    def getRecord(self, date):
        data = list()
        records = self.engine.rh.getRecordsByDate(date)
        for i in records:
            data.append({u'id': i.id, u'name': i.event, u'size': i.size})
        return data

    def getAllRecords(self):
        return self.engine.rh.getAllRecords()

    def startRecordJob(self, drive_id, records):
        self.engine.jh.addRecordJob(drive_id, records)

    def getAllMessages(self):
        return self.engine.mh.getAllMessages()

    def checkRecordJob(self, drive_id, records_id_list):
        return self.engine.jh.checkRecordJob(drive_id, records_id_list)

    def cancelRecordJob(self, drive_id):
        job = self.engine.jh.getRecordJobByDriveId(drive_id)
        if job is not None:
            job.cancel()
            return True
        return False

    def checkDelete(self, drive_id):
        return self.engine.dh.checkDelete(drive_id)

    def deleteDrive(self, drive_id):
        drive = self.engine.dh.getDriveById(drive_id)
        if isinstance(drive, Drive):
            drive.start_delete_thread()

    def stopDeleteDrive(self, drive_id):
        drive = self.engine.dh.getDriveById(drive_id)
        if drive is not None:
            drive.stop_deleting()

    def reloadDhSettings(self):
        self.engine.dh.load_drivesettings()  # todo testen

    def reloadTimeSettings(self):
        self.engine.master.th.reload_timer()

    def lock(self, session_id):
        temp = self.engine.sh.checkRight(session_id, EncrConst.UNLOCK)
        if temp[0]:
            self.engine.master.th.lock_manually()
        else:
            self.engine.mh.addMessage(temp[1], level=MDConst.MESSAGE_WARNING, requested=False)

    def unlock(self, session_id):
        temp = self.engine.sh.checkRight(session_id, EncrConst.UNLOCK)
        if temp[0]:
            self.engine.master.th.unlock()

    def isLocked(self):
        return self.engine.master.th.is_locked

    def reloadRecords(self):
        self.engine.rh.request_scan = True

    def getAllInfos(self):
        return self.engine.master.i.get_all_infos()

    def set_auto_shutdown(self, session_id):
        if self.engine.sh.checkRight(session_id, EncrConst.SHUTDOWN)[0]:
            self.engine.master.th.set_auto_shutdown()
            return True
        else:
            return False

    def unset_auto_shutdown(self, session_id):
        if self.engine.sh.checkRight(session_id, EncrConst.SHUTDOWN)[0]:
            self.engine.master.th.unset_auto_shutdown()
            return True
        else:
            return False

    def logOn(self, name, password):
        return self.engine.sh.logOn(name, password)

    def logOff(self, session_id):
        self.engine.sh.logOffById(session_id)

    def checkSession(self, session_id):
        session = self.engine.sh.getSessionById(session_id)
        if session:
            return session.name
        else:
            return u''

    def quit(self, session_id):
        temp = self.engine.sh.checkRight(session_id, EncrConst.QUIT)
        if temp[0]:
            self.engine.master.th.request_quit()
        else:
            self.engine.mh.addMessage(temp[1], level=MDConst.MESSAGE_WARNING, requested=False)

    def shutdown(self, session_id):
        temp = self.engine.sh.checkRight(session_id, EncrConst.SHUTDOWN)
        if temp[0]:
            self.engine.master.th.request_shutdown()
        else:
            self.engine.mh.addMessage(temp[1], level=MDConst.MESSAGE_WARNING, requested=False)

    def get_state_html(self):
        state = self.engine.master.th.state
        if state in (0, 1, 2, 5):
            return u''
        elif state == 3:
            return u'<div class="panel panel-danger"><div class="panel-heading"><h3 class="panel-title">Hinweis</h3>' \
                   u'</div><div class="panel-body">Der Rechner wird nach Beendigung aller Aufträge heruntergefahren.' \
                   u'<br><button type="button" onclick="send_ajax_request(\'cancel_sd\')" class="btn btn-default">' \
                   u'Abbrechen</button></div></div>'
        elif state == 7:
            return u'<div class="panel panel-danger"><div class="panel-heading"><h3 class="panel-title">Hinweis</h3>' \
                   u'</div><div class="panel-body">Der Rechner wird gleich heruntergefahren.' \
                   u'</div></div>'
        elif state == 8:
            return u'<div class="panel panel-danger"><div class="panel-heading"><h3 class="panel-title">Hinweis</h3>' \
                   u'</div><div class="panel-body">Das Programm wird nach Beendigung aller Aufträge beendet.<br>' \
                   u'<button type="button" onclick="send_ajax_request(\'cancel_quit\')" class="btn btn-default">' \
                   u'Abbrechen</button></div></div>'
        elif state == 9:
            return u'<div class="panel panel-danger"><div class="panel-heading"><h3 class="panel-title">Hinweis</h3>' \
                   u'</div><div class="panel-body">Das Programm wird gleich beendet.' \
                   u'</div></div>'
        elif state == 6:
            return u'<div class="panel panel-danger"><div class="panel-heading"><h3 class="panel-title">Hinweis</h3>' \
                   u'</div><div class="panel-body">Der Rechner wird in %s heruntergefahren.<br><button type="button" ' \
                   u'onclick="send_ajax_request(\'cancel_sd_start\')" class="btn btn-default">Abbrechen' \
                   u'</button></div></div>' % self.engine.master.th.get_left_time()
        elif state == 4:
            return u'<div class="panel panel-danger"><div class="panel-heading"><h3 class="panel-title">Hinweis</h3>' \
                   u'</div><div class="panel-body">Der Rechner wird in %s heruntergefahren.<br><button type="button" ' \
                   u'onclick="send_ajax_request(\'cancel_sd_timer\')" class="btn btn-default">Verschieben</button>' \
                   u'</div></div>' % self.engine.master.th.get_left_time()
        return u'<div class="panel panel-danger"><div class="panel-heading"><h3 class="panel-title">Hinweis</h3>' \
               u'</div><div class="panel-body">Unbekannter Zustand.</div></div>'

    def cancel_sd_timer(self):
        self.engine.master.th.set_run_timer()

    def cancel_sd_start(self):
        self.engine.master.th.cancel_sd_start()

    def get_fsm_state(self):
        return self.engine.master.th.state

    def cancel_sd(self):
        self.engine.master.th.set_run_timer()

    def cancel_quit(self):
        self.engine.master.th.set_run_timer()

    def set_run(self, session_id):
        check = self.engine.sh.checkRight(session_id, EncrConst.MANIPULATE_FSM)
        if check[0]:
            self.engine.master.th.set_run()
        else:
            self.engine.mh.addMessage(check[1], level=MDConst.MESSAGE_WARNING, requested=False)

    def get_records_stat(self, session_id):
        check = self.engine.sh.checkRight(session_id, EncrConst.STATISTICS)
        if check[0]:
            html = u'<table  class="table table-striped">'
            html += u'<tbody>'
            for i, j in self.engine.stat.getStatCopyRecords():  # todo try/ catch
                html += u'<tr><td>%s</td><td>%i</td></tr>' % (i, j)
            html += u'</tbody></table>'
            return html
        else:
            return u'Sie haben keine Berechtigung diese Daten zu sehen: %s' % check[1]

    def get_records_stat_month_table(self, session_id, month, year):
        check = self.engine.sh.checkRight(session_id, EncrConst.STATISTICS)
        if check[0]:
            data = self.engine.stat.getStatCopyRecordsByDay()
            temp = calendar.monthrange(year, month)
            month_range = range(1, temp[1] + 1)
            daily_stats = list()
            html_body = ''
            for day in month_range:
                date = datetime.datetime(year, month, day, hour=12)
                delta = date - MDConst.EPOCH_TIME
                new_time = delta.total_seconds()
                html_body += '<h3>%s %s</h3>\n' % (day, DAY_DICT_GE[calendar.weekday(year, month, day)])
                if new_time in data:
                    counter = 0
                    temp_html = 'Gesamt: %i<br>\n'
                    table_html = '<table class="table table-striped" style="width:100%">\n' \
                                 '<tr>\n<th>Anzahl</th>' \
                                 '<th>Ordner</th>\n</tr>'
                    for key, value in data[new_time].iteritems():
                        table_html += '<tr> <td>%i</td> <td>%s</td> </tr>' % (value, key)
                        counter += value
                    daily_stats.append(counter)
                    table_html += '</table>'
                    html_body += temp_html % counter + table_html
                else:
                    daily_stats.append(0)
                html_body += '<hr>\n'

            return html_body
        else:
            return u'Sie haben keine Berechtigung diese Daten zu sehen: %s' % check[1]

    def get_files_stat(self, session_id):
        check = self.engine.sh.checkRight(session_id, EncrConst.STATISTICS)
        if check[0]:
            html = u'<table  class="table table-striped">'
            html += u'<tbody>'
            for i, j in self.engine.stat.getStatCopyFiles():  # todo try/ catch
                html += u'<tr><td>%s</td><td>%i</td></tr>' % (i, j)
            html += u'</tbody></table>'
            return html
        else:
            return u'Sie haben keine Berechtigung diese Daten zu sehen: %s ' % check[1]

    def get_calendar_stat(self, session_id):
        check = self.engine.sh.checkRight(session_id, EncrConst.STATISTICS)
        if check[0]:
            data = self.engine.stat.getStatCopyRecordsByDay()
            new_data = []
            for key, value in data.iteritems():
                counter = 0
                for key2, value2 in value.iteritems():
                    counter += value2
                new_data.append([key * 1000, counter])
            return check[0], new_data
        else:
            return check[0], u'Sie haben keine Berechtigung diese Daten zu sehen: %s ' % check[1]

    def getChurches(self):
        return self.engine.rh.getChurches()

    def showNormal(self, session_id):
        check = self.engine.sh.checkRight(session_id, EncrConst.SHOWNORMAL)
        if check[0]:
            self.engine.master.b.b.showNormal()
        else:
            self.engine.mh.addMessage(check[1], level=MDConst.MESSAGE_WARNING, requested=False)

    def showFullScreen(self):
        self.engine.master.b.b.showFullScreen()

    def orderCD(self, record_date, record_name, name, church, cd_format=None, comment=None, count=None):
        row_id = None
        try:
            row_id = self.engine.coh.order(record_date, record_name, name, church, cd_format, comment, count)
        except Exception as e:
            self.engine.l.logEngine(u'Datenbankzugriff bei Anlegen von CD Bestellung fehlgeschlagen: %s' % e.message,
                                    logLevel=MDConst.LOG_ERROR)
            self.engine.mh.addMessage(u'Datenbankzugriff bei Anlegen von CD Bestellung fehlgeschlagen: %s' % e.message,
                                      level=MDConst.MESSAGE_ERROR)
            self.engine.mh.addMessage(u'Datenbankzugriff fehlgeschlagen. Weitere Details in der Log-Datei.',
                                      level=MDConst.MESSAGE_ERROR, requested=False)
        return row_id

    def getCDFormats(self):
        data = {}
        try:
            data = self.engine.coh.get_formats()
        except Exception as e:
            self.engine.l.logEngine(u'Datenbankzugriff bei Abruf von CD Formaten fehlgeschlagen: %s' % e.message,
                                    logLevel=MDConst.LOG_ERROR)
            self.engine.mh.addMessage(u'Datenbankzugriff bei Abruf von CD Formaten fehlgeschlagen: %s' % e.message,
                                      level=MDConst.MESSAGE_ERROR)
            self.engine.mh.addMessage(u'Datenbankzugriff fehlgeschlagen. Weitere Details in der Log-Datei.',
                                      level=MDConst.MESSAGE_ERROR, requested=False)
        return data

    def getCDOrderActivated(self):
        return self.engine.coh.get_activated()

    def getCDOrders(self, state):
        data = {}
        try:
            data = self.engine.coh.get_orders(state)
        except Exception as e:
            self.engine.l.logEngine(u'Datenbankzugriff bei Abruf von CD Aufträgen fehlgeschlagen: %s' % e.message,
                                    logLevel=MDConst.LOG_ERROR)
            self.engine.mh.addMessage(u'Datenbankzugriff bei Abruf von CD Aufträgen fehlgeschlagen: %s' % e.message,
                                      level=MDConst.MESSAGE_ERROR)
            self.engine.mh.addMessage(u'Datenbankzugriff fehlgeschlagen. Weitere Details in der Log-Datei.',
                                      level=MDConst.MESSAGE_ERROR, requested=False)
        return data

    def getGeneralSettings(self, session_id):
        check = self.engine.sh.checkRight(session_id, EncrConst.SETTINGS)
        if not check[0]:
            self.engine.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] + u'.',
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
            return {}
        return self.engine.master.sth.get_general_settings()

    def checkGeneralSettings(self, settings, session_id):
        check = self.engine.sh.checkRight(session_id, EncrConst.SETTINGS)
        if not check[0]:
            self.engine.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] + u'.',
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
            return {}, [check[1]]
        new_settings, messages = self.engine.master.sth.check_general_settings(settings)
        if messages:
            html = u'<h4> Folgende Einstellungen sind fehlerhaft: </h4><ul>'
            for message in messages:
                html += u'<li>' + message + u'</li>'
            html += u'</ul>'
            self.engine.mh.addMessage(html, level=MDConst.MESSAGE_WARNING, requested=False)
        else:
            self.engine.mh.addMessage(u'Alles in Ordnung', level=MDConst.MESSAGE_INFO, requested=False)
        return new_settings, messages

    def getGeneralDefaultSettings(self, session_id):
        check = self.engine.sh.checkRight(session_id, EncrConst.SETTINGS)
        if not check[0]:
            self.engine.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] + u'.',
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
            return {}
        return DefaultSettings.general

    def setGeneralSettings(self, settings, session_id):
        check = self.engine.sh.checkRight(session_id, EncrConst.SETTINGS)
        if not check[0]:
            self.engine.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] + u'.',
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
            return {}, [check[1]]
        new_settings, messages = self.engine.master.sth.check_general_settings(settings)
        self.engine.master.sth.general_settings = new_settings
        self.engine.master.sth.write_general_settings()
        MDConst.events.general_load_settings.emit()
        if messages:
            html = u'Einstellungen wurden korrigiert und gespeichert.'
            self.engine.mh.addMessage(html, level=MDConst.MESSAGE_WARNING, requested=False)
        else:
            html = u'Einstellungen wurden gespeichert.'
            self.engine.mh.addMessage(html, level=MDConst.MESSAGE_INFO, requested=False)
        return new_settings, messages

    def getExistingDir(self, session_id, default_dir=None):
        check = self.engine.sh.checkRight(session_id, EncrConst.SETTINGS)
        if not check[0]:
            self.engine.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] + u'.',
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
            return None
        return self.engine.master.b.getExistingDir(default_dir)

    def getFileSettings(self, session_id):
        check = self.engine.sh.checkRight(session_id, EncrConst.SETTINGS)
        if not check[0]:
            self.engine.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] + u'.',
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
            return {}
        return self.engine.master.sth.get_files_settings()

    def checkFileSettings(self, settings, session_id):
        check = self.engine.sh.checkRight(session_id, EncrConst.SETTINGS)
        if not check[0]:
            self.engine.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] + u'.',
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
            return {}, [check[1]]
        new_settings, messages = self.engine.master.sth.check_files_settings(settings)
        if messages:
            html = u'<h4> Folgende Einstellungen sind fehlerhaft: </h4><ul>'
            for message in messages:
                html += u'<li>' + message + u'</li>'
            html += u'</ul>'
            self.engine.mh.addMessage(html, level=MDConst.MESSAGE_WARNING, requested=False)
        else:
            self.engine.mh.addMessage(u'Alles in Ordnung', level=MDConst.MESSAGE_INFO, requested=False)
        return new_settings, messages

    def setFileSettings(self, settings, session_id):
        check = self.engine.sh.checkRight(session_id, EncrConst.SETTINGS)
        if not check[0]:
            self.engine.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] + u'.',
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
            return {}, [check[1]]
        new_settings, messages = self.engine.master.sth.check_files_settings(settings)
        self.engine.master.sth.files_settings = new_settings
        self.engine.master.sth.write_files_settings()
        MDConst.events.files_load_settings.emit()
        if messages:
            html = u'Einstellungen wurden korrigiert und gespeichert.'
            self.engine.mh.addMessage(html, level=MDConst.MESSAGE_WARNING, requested=False)
        else:
            html = u'Einstellungen wurden gespeichert.'
            self.engine.mh.addMessage(html, level=MDConst.MESSAGE_INFO, requested=False)
        return new_settings, messages

    def getFileDefaultSettings(self, session_id):
        check = self.engine.sh.checkRight(session_id, EncrConst.SETTINGS)
        if not check[0]:
            self.engine.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] + u'.',
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
            return {}
        return DefaultSettings.files

    def getDbSettings(self, session_id):
        check = self.engine.sh.checkRight(session_id, EncrConst.SETTINGS)
        if not check[0]:
            self.engine.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] + u'.',
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
            return {}
        return self.engine.master.sth.get_db_settings()

    def checkDbSettings(self, settings, session_id):
        check = self.engine.sh.checkRight(session_id, EncrConst.SETTINGS)
        if not check[0]:
            self.engine.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] + u'.',
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
            return {}, [check[1]]
        new_settings, messages = self.engine.master.sth.check_db_settings(settings)
        if messages:
            html = u'<h4> Folgende Einstellungen sind fehlerhaft: </h4><ul>'
            for message in messages:
                html += u'<li>' + message + u'</li>'
            html += u'</ul>'
            self.engine.mh.addMessage(html, level=MDConst.MESSAGE_WARNING, requested=False)
        else:
            self.engine.mh.addMessage(u'Alles in Ordnung', level=MDConst.MESSAGE_INFO, requested=False)
        return new_settings, messages

    def setDbSettings(self, settings, session_id):
        check = self.engine.sh.checkRight(session_id, EncrConst.SETTINGS)
        if not check[0]:
            self.engine.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] + u'.',
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
            return {}, [check[1]]
        new_settings, messages = self.engine.master.sth.check_db_settings(settings)
        self.engine.master.sth.db_settings = new_settings
        self.engine.master.sth.write_db_settings()
        MDConst.events.db_load_settings.emit()
        if messages:
            html = u'Einstellungen wurden korrigiert und gespeichert.'
            self.engine.mh.addMessage(html, level=MDConst.MESSAGE_WARNING, requested=False)
        else:
            html = u'Einstellungen wurden gespeichert.'
            self.engine.mh.addMessage(html, level=MDConst.MESSAGE_INFO, requested=False)
        return new_settings, messages

    def getDbDefaultSettings(self, session_id):
        check = self.engine.sh.checkRight(session_id, EncrConst.SETTINGS)
        if not check[0]:
            self.engine.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] + u'.',
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
            return {}
        return DefaultSettings.db

    def checkDbConnection(self, connection_settings, session_id):
        check = self.engine.sh.checkRight(session_id, EncrConst.SETTINGS)
        if not check[0]:
            self.engine.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] + u'.',
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
            return False
        new_settings, messages = self.engine.master.sth.check_db_settings(connection_settings)
        if messages:
            html = u'<h4> Folgende Einstellungen sind fehlerhaft: </h4><ul>'
            for message in messages:
                html += u'<li>' + message + u'</li>'
            html += u'</ul>'
            self.engine.mh.addMessage(html, level=MDConst.MESSAGE_WARNING, requested=False)
            return False
        else:
            return CDOrderHandler.check_db_connection(new_settings)

    def getDefaultChurch(self):
        return self.engine.rh.get_default_church()

    def deleteStatistics(self, session_id):
        check = self.engine.sh.checkRight(session_id, EncrConst.STATISTICS)
        if not check[0]:
            self.engine.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] + u'.',
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
        error_list = self.engine.stat.deleteStat()
        if error_list:
            self.engine.master.mh.addMessage(u'Beim Löschen der Statistik traten %i Fehler auf. Für weitere '
                                             u'Informationen siehe Logfile.' % len(error_list),
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
        else:
            self.engine.master.mh.addMessage(u'Statistik erfolgreich gelöscht.',
                                             level=MDConst.MESSAGE_INFO,
                                             requested=False)

    def getUsersData(self, session_id):
        return self.engine.sh.pws.getUsersData(session_id)

    def changePassword(self, session_id, user_name, password1, password2):
        return self.engine.sh.pws.changePw(session_id, user_name, password1, password2)

    def addUser(self, session_id, user_name, password1, password2, rights):
        return self.engine.sh.pws.addUser(session_id, user_name, password1, password2, rights)

    def deleteUser(self, session_id, user_name):
        return self.engine.sh.pws.deleteUser(session_id, user_name)

    def setUserRights(self, session_id, user_name, rights):
        return self.engine.sh.pws.setUserRights(session_id, user_name, rights)
