# -*- coding: utf-8 -*-

import threading
import time
import traceback

import MDConst
from CDOrders import CDOrderHandler
from DriveHandling import DriveHandler
from FileHandling import FileHandler
from FileHandling import RecordHandler
from ServerInterface import ServerInterface
from SessionHandling import SessionHandler
from Statistics import Statistics
from JobHandling import JobHandler


class Engine(object):
    def __init__(self, master, logger, message_handler):
        self.master = master
        self.run = True
        self.l = logger
        self.mh = message_handler
        self.fh = FileHandler(logger, self)
        self.dh = DriveHandler(self)
        self.jh = JobHandler(self)
        self.si = ServerInterface(self)
        self.rh = RecordHandler(self)
        self.sh = SessionHandler(self)
        self.stat = Statistics(self)
        self.coh = CDOrderHandler(self)
        self.update_drive_pause = 0
        self.counter_update_dh = 0
        self.counter_update_jh = 0
        self.counter_update_mh = 0
        self.counter_update_rh = 0
        self.counter_update_sh = 0
        self.counter_update_stat = 0
        self.update_error_dh = False
        self.update_error_mh = False
        self.update_error = False
        self.thread = threading.Thread(target=self.update, args=(), name=u'EngineUpdater')
        self.thread.daemon = True

    def getThreadRunning(self):
        if not self.thread.is_alive() and self.run:
            return False
        return True

    def restart(self):
        if not self.thread.is_alive():
            self.thread = threading.Thread(target=self.update, args=(), name=u'EngineUpdater')
            self.thread.daemon = True
            self.thread.start()

    def update(self):
        self.l.logEngine(u'Start Engine', logLevel=MDConst.LOG_DEBUG)
        i = 0
        while self.run:
            if not self.update_drive_pause and not self.counter_update_dh:
                try:
                    self.dh.update()
                except Exception as e:
                    if not MDConst.FROZEN:
                        print e
                        print traceback.format_exc()
                    self.l.logEngine(u'Update DriveHandler failed: %s' % e, logLevel=MDConst.LOG_ERROR)
                    self.mh.addMessage(u'Update DriveHandler failed %s' % e, level=MDConst.MESSAGE_ERROR)
            if not self.counter_update_jh:
                try:
                    self.jh.update()
                except Exception as e:
                    self.l.logEngine(u'Update JobHandler failed: %s' % e, logLevel=MDConst.LOG_ERROR)
                    self.mh.addMessage(u'Update JobHandler failed: %s' % e, level=MDConst.MESSAGE_ERROR)
            if not self.counter_update_mh:
                try:
                    self.mh.update()
                except Exception as e:
                    self.l.logEngine(u'Update MessageHandler failed: %s' % e, logLevel=MDConst.LOG_ERROR)
                    self.mh.addMessage(u'Update MessageHandler failed: %s' % e, level=MDConst.MESSAGE_ERROR)
            '''
            if not self.counter_update_rh:
                #self.l.logEngine('Update RecordHandler')
                try:
                    self.rh.update()
                except Exception as e:
                    self.l.logEngine(u'Update RecordHandler failed: %s' % e, logLevel=MDConst.LOG_ERROR)
                    self.mh.addMessage(u'Update RecordHandler failed: %s' % e, level=MDConst.MESSAGE_ERROR)
            '''
            if not self.counter_update_sh:
                try:
                    self.sh.update()
                except Exception as e:
                    self.l.logEngine(u'Update SessionHandler failed: %s' % e, logLevel=MDConst.LOG_ERROR)
                    self.mh.addMessage(u'Update SessionHandler failed: %s' % e, level=MDConst.MESSAGE_ERROR)
            if not self.counter_update_stat:
                try:
                    self.stat.flush()
                except Exception as e:
                    self.l.logEngine(u'Flush Statisitcs failed: %s' % e, logLevel=MDConst.LOG_ERROR)
                    self.mh.addMessage(u'Flush Statisitcs failed: %s' % e, level=MDConst.MESSAGE_ERROR)

            self.counter_update_dh += 1
            self.counter_update_dh %= MDConst.UPDATE_TIME_DH
            self.counter_update_jh += 1
            self.counter_update_jh %= MDConst.UPDATE_TIME_JH
            self.counter_update_mh += 1
            self.counter_update_mh %= MDConst.UPDATE_TIME_MH
            self.counter_update_rh += 1
            self.counter_update_rh %= MDConst.UPDATE_TIME_RH
            self.counter_update_sh += 1
            self.counter_update_sh %= MDConst.UPDATE_TIME_SH
            self.counter_update_stat += 1
            self.counter_update_stat %= MDConst.UPDATE_TIME_STAT

            i += 1
            i %= 200

            time.sleep(0.1)

    '''
    def stop(self):
        if self.jh.numberOfRunningJobs():
            self.jh.deleteAllJobs()
        if self.jh.numberOfJobs():
            return False  # todo
        else:
            self.run = False  # todo muss solange updaten, bis jobhandler keine Jobs mehr hat
            return True
    '''

    def start(self):
        # update all
        self.rh.start()
        try:
            self.dh.update()
        except Exception as e:
            self.l.logEngine(u'Update DriveHandler failed: %s' % e, logLevel=MDConst.LOG_ERROR)
            self.mh.addMessage(u'Update DriveHandler failed %s' % e, level=MDConst.MESSAGE_ERROR)
        try:
            self.jh.update()
        except Exception as e:
            self.l.logEngine(u'Update JobHandler failed: %s' % e, logLevel=MDConst.LOG_ERROR)
            self.mh.addMessage(u'Update JobHandler failed: %s' % e, level=MDConst.MESSAGE_ERROR)
        try:
            self.mh.update()
        except Exception as e:
            self.l.logEngine(u'Update MessageHandler failed: %s' % e, logLevel=MDConst.LOG_ERROR)
            self.mh.addMessage(u'Update MessageHandler failed: %s' % e, level=MDConst.MESSAGE_ERROR)
        try:
            self.sh.update()
        except Exception as e:
            self.l.logEngine(u'Update SessionHandler failed: %s' % e, logLevel=MDConst.LOG_ERROR)
            self.mh.addMessage(u'Update SessionHandler failed: %s' % e, level=MDConst.MESSAGE_ERROR)

        self.thread.start()

    def is_stop_ready(self):
        if self.jh.numberOfJobs() > 0:
            return False
        if self.dh.has_delete_tasks():
            return False
        return True
