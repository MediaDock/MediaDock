# -*- coding: utf-8 -*-


import codecs
import os
import time
import json
from PySide.QtCore import QObject, Slot

import JsScripts
import MDConst
import EncrConst


class GuiInterface(object):
    def __init__(self, master):
        self.master = master
        self.ps = PServer(self)
        self.jsi = JSInterface(self)
        self.pah = PageHandler(self, self.ps)


class JSInterface(object):
    def __init__(self, master):
        self.master = master
        self.return_values = {}

    def executeJs(self, js, return_value=False, timeout=10):
        if not return_value:
            return_id = None
        else:
            i = 0
            while i in self.return_values:
                i += 1
            return_id = i
            self.return_values[return_id] = NoClass()
        self.master.master.b.execJS(js, return_id, self.return_values)
        if return_id is not None:
            wait_time = 0.001
            time_counter = int(timeout / wait_time)
            for x in range(time_counter):
                time.sleep(wait_time)
                value = self.return_values[return_id]
                if not isinstance(value, NoClass):
                    del self.return_values[return_id]
                    return value
            raise


class Page(object):
    replacer = {}
    for root, dirs, files in os.walk(os.path.join(u'HTML', u'static')):
        for i in files:
            relpath = os.path.join(root, i)
            abspath = os.path.abspath(relpath)
            replacer[relpath[5:].replace(os.sep, '/')] = u'file:///' + abspath.replace(os.sep, '/').lstrip('/')

    def __init__(self, name, filename, need_server=False, js_files=None, js_init=None):
        if js_files is None:
            js_files = ()
        if js_init is None:
            self.jsInit = ''
        else:
            self.jsInit = js_init
        self.name = name
        self.needServer = need_server
        self.jsFiles = js_files

        html_file = codecs.open(filename, 'r', 'utf-8')
        content = html_file.read()
        html_file.close()
        # print re.search('<script>.*</script>', content)

        for key, value in self.replacer.iteritems():
            content = content.replace(key, value)

        self.html = content


class PageHandler(object):
    pages = {
        'index': Page('index', os.path.join(u'HTML', u'index.html'), js_files=[JsScripts.MediaDockLibrary,
                                                                               JsScripts.modalMessages,
                                                                               JsScripts.check_locked,
                                                                               JsScripts.calendar, JsScripts.session],
                      js_init=JsScripts.index_init),
        'menu': Page('menu', os.path.join(u'HTML', u'menu.html'), js_init=JsScripts.menu_init),
        'about': Page('about', os.path.join(u'HTML', u'about.html'), js_init=JsScripts.about_init),
        'collections': Page('collections', os.path.join(u'HTML', u'collections.html'),
                            js_init=JsScripts.collections_init),
        'default': Page('default', os.path.join(u'HTML', u'default.html'), js_init=JsScripts.default_init),
        'drives': Page('drives', os.path.join(u'HTML', u'drives.html'), js_init=JsScripts.drives_init),
        'explorer': Page('explorer', os.path.join(u'HTML', u'explorer.html'), js_init=JsScripts.explorer_init),
        'fsm': Page('fsm', os.path.join(u'HTML', u'fsm.html'), js_init=JsScripts.fsm_init),
        'info': Page('info', os.path.join(u'HTML', u'info.html'), js_init=JsScripts.info_init),
        'license': Page('license', os.path.join(u'HTML', u'license.html'), js_init=JsScripts.license_init),
        'lock': Page('lock', os.path.join(u'HTML', u'lock.html'), js_init=JsScripts.lock_init),
        'messages': Page('messages', os.path.join(u'HTML', u'messages.html'), js_init=JsScripts.messages_init),
        'search': Page('search', os.path.join(u'HTML', u'search.html'), js_init=JsScripts.search_init),
        'service': Page('service', os.path.join(u'HTML', u'service.html'), js_init=JsScripts.service_init),
        'settings': Page('settings', os.path.join(u'HTML', u'settings.html'), js_init=JsScripts.settings_init),
        'statistics': Page('statistics', os.path.join(u'HTML', u'statistics.html'), js_init=JsScripts.statistics_init),
        'terms_of_use': Page('terms_of_use', os.path.join(u'HTML', u'terms_of_use.html'),
                             js_init=JsScripts.terms_or_use_init),
        'cd_orders': Page('cd_orders', os.path.join(u'HTML', u'cd_orders.html'), js_init=JsScripts.cd_orders_init),
        'users': Page('users', os.path.join(u'HTML', u'users.html'), js_init=JsScripts.users_init),
    }

    def __init__(self, master, server):
        self.master = master
        self.server = server
        self.previous = []
        self.page = 'index'

    def loadPage(self, par_page, session_id=None):
        if par_page in ('settings',):
            check = self.master.master.e.sh.checkRight(session_id, EncrConst.SETTINGS)
            if not check[0]:
                self.master.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] +
                                                 u'. Es wird der Kalender geladen.', level=MDConst.MESSAGE_ERROR,
                                                 requested=False)
                par_page = 'index'
        if par_page == 'users':
            if not self.master.master.e.sh.checkAnyLogon():
                self.master.master.mh.addMessage(u'Ein Fehler ist auf getreten: Kein Nutzer angemeldet. '
                                                 u'Es wird der Kalender geladen.', level=MDConst.MESSAGE_ERROR,
                                                 requested=False)
                par_page = 'index'
        if par_page not in self.pages:
            self.master.master.mh.addMessage(u'Die angeforderte Seite konnte nicht gefunden werden. Es wird der '
                                             u'Kalender geladen.', level=MDConst.MESSAGE_ERROR, requested=False)
            par_page = 'index'
        page = self.pages[par_page]
        # self.master.master.b.execJS('clear();')
        self.master.master.b.setContent(page.html)
        self.master.master.b.b.html_viewer.page().mainFrame().addToJavaScriptWindowObject('pserver', self.server)
        # time.sleep(2)
        '''
        for x in page.jsFiles:
            #print x
            self.master.master.b.execJS(x)
        '''
        for i in page.jsInit.split('\n'):
            self.master.jsi.executeJs(i)
            # print self.master.jsi.executeJs(i, return_value=True), '###', i

        self.page = page.name

    def loadStartPage(self):
        self.loadPage('index')


class NoClass(object):
    pass


class PServer(QObject):
    def __init__(self, master):
        super(PServer, self).__init__()
        self.master = master
        self.ei = master.master.e.si
        self.session_id = None
        if not MDConst.FROZEN:
            check, message, self.session_id = self.ei.logOn(u'admin', u'administrator')

    @staticmethod
    def encodeAjax(call_str):
        '''print call_str, 'call str'
        for i in call_str:
            print i
        if not '&' in call_str and not '=' in call_str:
            return {'function': call_str}
        d = dict()
        for i in call_str.split('&'):
            data = i.split('=')
            d[data[0]] =  data[1]'''
        d = json.loads(call_str)
        # print 'd', d, type(d)
        if not isinstance(d, dict):
            raise
        if 'function' not in d:
            raise
        # print 'd', d, type(d)
        return d

    @Slot(str, str)
    def send(self, func, args):
        # print args
        # args = json.loads(args)
        # print type(args)
        if not MDConst.FROZEN:
            try:
                print 'request ', func, args
            except:
                print 'request unknown'
        if func == 'getTime':
            pass
        elif func == 'print':
            print args
        elif func == 'loadPage':
            self.master.pah.loadPage(args, session_id=self.session_id)
        elif func == 'execJs':
            if not MDConst.FROZEN:
                temp = self.master.jsi.executeJs('alert(%s)' % args, True)
                # print 'execJs', temp
        elif func == 'log':
            data = json.loads(args)
            self.master.master.l.log('GuiInterface', data['message'], data['level'])
        elif func == 'ajax':
            parameters = self.encodeAjax(args)
            # print parameters
            json_answer = json.dumps({'state': 1})
            func = parameters['function']
            if func == 'getVolumes':
                json_answer = json.dumps({"status": 1, "info": self.ei.getDriveMessagesSidebar()})
            elif func == 'getMessages':
                json_answer = json.dumps({"status": 1, "info": self.ei.getMessageMessagesSidebar()})
            elif func == 'getModalMessages':
                info = self.ei.getModalMessages()
                json_answer = json.dumps({"status": 1, 'info': info})
            elif func == 'getJobs':
                json_answer = json.dumps({"status": 1, "info": self.ei.getJobMessagesSidebar()})
            elif func == 'getDrives':
                json_answer = json.dumps({"status": 1, "info": self.ei.getDrives()})
            elif func == 'getDriveMessagesDrive':
                json_answer = json.dumps({"status": 1, "info": self.ei.getDriveMessagesDrives()})
            elif func == 'getExplorerDropdown':
                json_answer = json.dumps({"status": 1, "info": self.ei.getExplorerDropdown()})
            elif func == 'explorerGetDir':
                info, path_above, record_data = self.ei.explorerGetDir(parameters['path'])
                json_answer = json.dumps({"status": 1, "info": info, "prev_path": path_above,
                                          "record_data": record_data})
            elif func.startswith('eject_drive'):
                self.ei.ejectDrive(func.split('_')[2])
                json_answer = json.dumps({"status": 1, "info": ""})
            elif func == 'quit':
                self.ei.quit(self.session_id)
                json_answer = json.dumps({'status': 1})
            elif func == 'shutdown':
                self.ei.shutdown(self.session_id)
                json_answer = json.dumps({'status': 1})
            elif func == 'reload_filesettings':
                self.ei.engine.fh.load_filesettings()  # todo remove engine
                json_answer = ''
            elif func == 'getCompoundData':
                json_answer = json.dumps({"status": 1, "info": self.ei.getCompoundData()})
            elif func == 'lock':
                self.ei.lock(self.session_id)
                json_answer = json.dumps({"status": 1})
            elif func == 'unlock':
                self.ei.unlock(self.session_id)
                json_answer = json.dumps({'status': 1})
            elif func == 'getRecord':
                json_answer = json.dumps({"status": 1, "info": self.ei.getRecord(parameters['date'])})
            elif func == 'getAllRecords':
                json_answer = json.dumps({"status": 1, "info": self.ei.getAllRecords()})
            elif func == 'startRecordJob':

                records_list = parameters['records']
                check_result = self.ei.checkRecordJob(int(parameters['drive_id']), records_list)
                # if check_result[0]:
                self.ei.startRecordJob(int(parameters['drive_id']), records_list)
                json_answer = json.dumps({"status": 1, 'check': str(check_result[0]), 'message': check_result[1]})

            elif func == 'getAllMessages':
                json_answer = json.dumps({"status": 1, "info": self.ei.getAllMessages()})
            elif func == 'cancelRecordJob':
                try:
                    drive_id_int = int(parameters['drive_id'])
                except ValueError:
                    answer = False
                else:
                    answer = self.ei.cancelRecordJob(drive_id_int)
                json_answer = json.dumps({"status": 1, "info": answer})
            elif func == 'deleteDrive':
                try:
                    drive_id_int = int(parameters['drive_id'])
                except ValueError:
                    check_result = False, 'Fehler in der Laufwerks-ID'
                else:
                    check_result = self.ei.checkDelete(drive_id_int)
                    if check_result[0]:
                        self.ei.deleteDrive(drive_id_int)
                json_answer = json.dumps({"status": 1, 'check': check_result[0], 'message': check_result[1]})
            elif func == 'stopDeleteDrive':
                try:
                    drive_id_int = int(parameters['drive_id'])
                except ValueError:
                    pass
                else:
                    self.ei.stopDeleteDrive(drive_id_int)
                json_answer = json.dumps({"status": 1})
            elif func == 'reload_dh_settings':
                self.ei.reloadDhSettings()
                json_answer = json.dumps({"status": 1})
            elif func == 'reload_time_settings':
                self.ei.reloadTimeSettings()
                json_answer = json.dumps({"status": 1})
            elif func == 'isLocked':
                ans = self.ei.isLocked()
                json_answer = json.dumps({"status": 1, 'info': ans})
            elif func == 'reload_records':
                self.ei.reloadRecords()
                json_answer = json.dumps({"status": 1})
            elif func == 'getAllInfos':
                json_answer = json.dumps({"status": 1, 'info': self.ei.getAllInfos()})
            elif func == 'set_auto_shutdown':
                self.ei.set_auto_shutdown(self.session_id)
                json_answer = json.dumps({'status': 1})
            elif func == 'unset_auto_shutdown':
                self.ei.unset_auto_shutdown(self.session_id)
                json_answer = json.dumps({'status': 1})
            elif func == 'logOn':
                result, message, self.session_id = self.ei.logOn(parameters['name'], parameters['password'])
                json_answer = json.dumps({'status': 1, 'result': (result, message)})
            elif func == 'logOff':
                self.ei.logOff(self.session_id)
                self.session_id = None
                json_answer = json.dumps({'status': 1})
            elif func == 'checkSession':
                result = self.ei.checkSession(self.session_id)
                json_answer = json.dumps({'status': 1, 'info': result})
            elif func == 'onLocalhost':
                json_answer = json.dumps({'status': 1, 'info': True})
            elif func == 'checkState':
                json_answer = json.dumps({'status': 1, 'html': self.ei.get_state_html()})
            elif func == 'get_fsm_state':
                json_answer = json.dumps({'status': 1, 'state': self.ei.get_fsm_state()})
            elif func == 'get_fsm_image_path':
                state = self.ei.get_fsm_state()
                run_path = os.path.abspath('.')
                path = 'file:///' + run_path
                if state in range(11):
                    path = os.path.join(path, 'HTML/static/images/fsm/fsm%i.svg' % state)
                else:
                    path = os.path.join(path, 'HTML/static/images/fsm/fsm.svg')
                json_answer = json.dumps({'path': path})
            elif func == 'cancel_sd_start':
                self.ei.cancel_sd_start()
                json_answer = json.dumps({'status': 1})
            elif func == 'cancel_sd_timer':
                self.ei.cancel_sd_timer()
                json_answer = json.dumps({'status': 1})
            elif func == 'cancel_sd':
                self.ei.cancel_sd()
                json_answer = json.dumps({'status': 1})
            elif func == 'cancel_quit':
                self.ei.cancel_quit()
                json_answer = json.dumps({'status': 1})
            elif func == 'set_run':
                self.ei.set_run(self.session_id)
                json_answer = json.dumps({'status': 1})
            elif func == 'get_records_stat':
                info = self.ei.get_records_stat(self.session_id)
                json_answer = json.dumps({'status': 1, 'info': info})
            elif func == 'get_files_stat':
                info = self.ei.get_files_stat(self.session_id)
                json_answer = json.dumps({'status': 1, 'info': info})
            elif func == 'get_calendar_stat':
                info = self.ei.get_calendar_stat(self.session_id)
                json_answer = json.dumps({'status': 1, 'check': info[0], 'info': info[1]})
            elif func == 'get_records_stat_month_table':
                info = self.ei.get_records_stat_month_table(self.session_id, parameters['month'],
                                                            parameters['year'])
                json_answer = json.dumps({'status': 1, 'info': info})
            elif func == 'getChurches':
                json_answer = json.dumps({'info': self.ei.getChurches()})
            elif func == 'getDefaultChurch':
                json_answer = json.dumps({'info': self.ei.getDefaultChurch()})
            elif func == 'showNormal':
                self.ei.showNormal(self.session_id)
                json_answer = json.dumps({'status': 1})
            elif func == 'showFullScreen':
                self.ei.showFullScreen()
                json_answer = json.dumps({'status': 1})
            elif func == 'orderCD':
                result = self.ei.orderCD(record_date=parameters['record_date'], record_name=parameters['record_name'],
                                         name=parameters['name'], church=parameters['church'],
                                         cd_format=parameters['format'], comment=parameters['comment'],
                                         count=parameters['count'])
                json_answer = json.dumps({'status': 1, 'result': result})
            elif func == 'getCDFormats':
                json_answer = json.dumps({'status': 1, 'formats': self.ei.getCDFormats()})
            elif func == 'getCDOrderActivated':
                json_answer = json.dumps({'status': 1, 'result': self.ei.getCDOrderActivated()})
            elif func == 'getCDOrders':
                _data = self.ei.getCDOrders(parameters['state'])
                json_answer = json.dumps({'status': 1, 'data': _data})
            elif func == 'getGeneralSettings':
                json_answer = json.dumps({'status': 1, 'data': self.ei.getGeneralSettings(self.session_id)})
            elif func == 'checkGeneralSettings':
                settings = parameters['data']
                new_settings, messages = self.ei.checkGeneralSettings(settings, self.session_id)
                json_answer = json.dumps({'status': 1, 'messages': messages, 'settings': new_settings})
            elif func == 'getGeneralDefaultSettings':
                json_answer = json.dumps({'status': 1, 'data': self.ei.getGeneralDefaultSettings(self.session_id)})
            elif func == 'saveGeneralSettings':
                settings = parameters['data']
                new_settings, messages = self.ei.setGeneralSettings(settings, self.session_id)
                json_answer = json.dumps({'status': 1, 'messages': messages, 'settings': new_settings})
            elif func == 'deleteStatistics':
                self.ei.deleteStatistics(self.session_id)
                json_answer = json.dumps({'status': 1})
            elif func == 'getExistingDir':
                json_answer = json.dumps({'status': 1, 'dir': self.ei.getExistingDir(self.session_id)})
            elif func == 'getFileSettings':
                json_answer = json.dumps({'status': 1, 'data': self.ei.getFileSettings(self.session_id)})
            elif func == 'checkFileSettings':
                settings = parameters['data']
                new_settings, messages = self.ei.checkFileSettings(settings, self.session_id)
                json_answer = json.dumps({'status': 1, 'messages': messages, 'settings': new_settings})
            elif func == 'saveFileSettings':
                settings = parameters['data']
                new_settings, messages = self.ei.setFileSettings(settings, self.session_id)
                json_answer = json.dumps({'status': 1, 'messages': messages, 'settings': new_settings})
            elif func == 'getFileDefaultSettings':
                json_answer = json.dumps({'status': 1, 'data': self.ei.getFileDefaultSettings(self.session_id)})
            elif func == 'getDbSettings':
                json_answer = json.dumps({'status': 1, 'data': self.ei.getDbSettings(self.session_id)})
            elif func == 'checkDbSettings':
                settings = parameters['data']
                new_settings, messages = self.ei.checkDbSettings(settings, self.session_id)
                json_answer = json.dumps({'status': 1, 'messages': messages, 'settings': new_settings})
            elif func == 'getDbDefaultSettings':
                json_answer = json.dumps({'status': 1, 'data': self.ei.getDbDefaultSettings(self.session_id)})
            elif func == 'saveDbSettings':
                settings = parameters['data']
                new_settings, messages = self.ei.setDbSettings(settings, self.session_id)
                json_answer = json.dumps({'status': 1, 'messages': messages, 'settings': new_settings})
            elif func == 'checkDbConnection':
                settings = parameters['data']
                result = self.ei.checkDbConnection(settings, self.session_id)
                json_answer = json.dumps({'status': 1, 'result': result})
            elif func == 'getUsersData':
                result = dict()
                result['check'], result['data'] = self.ei.getUsersData(self.session_id)
                json_answer = json.dumps({'status': 1, 'result': result})
            elif func == 'changePassword':
                result = dict()
                result['check'], result['message'] = self.ei.changePassword(self.session_id, parameters['user_name'],
                                                                            parameters['password1'],
                                                                            parameters['password2'])
                json_answer = json.dumps({'status': 1, 'result': result})
            elif func == 'addUser':
                result = dict()
                result['check'], result['message'] = self.ei.addUser(self.session_id, parameters['user_name'],
                                                                            parameters['password1'],
                                                                            parameters['password2'],
                                                                            parameters['rights'])
                json_answer = json.dumps({'status': 1, 'result': result})
            elif func == 'deleteUser':
                result = dict()
                result['check'], result['message'] = self.ei.deleteUser(self.session_id, parameters['user_name'])
                json_answer = json.dumps({'status': 1, 'result': result})
            elif func == 'setUserRights':
                result = dict()
                result['check'], result['message'] = self.ei.setUserRights(self.session_id, parameters['user_name'],
                                                                           parameters['rights'])
                json_answer = json.dumps({'status': 1, 'result': result})
            else:
                self.ei.engine.l.logPServer(u'Server - Unbekannte Anfrage: ' + unicode(parameters),
                                            logLevel=MDConst.LOG_INFO)
                json_answer = json.dumps({"status": 1, "info": "Unbekannte Anfrage"})
            # print json_answer
            # return json_answer
            # print parameters, 'parameters'
            if 'return_id' in parameters:
                # print 'return now'
                js_string = 'return_object[%s] = %s;' % (parameters['return_id'], json_answer)
                # print js_string
                self.master.jsi.executeJs(js_string)

        else:
            print 'pserver unknown function', func, args
            pass  # todo message
