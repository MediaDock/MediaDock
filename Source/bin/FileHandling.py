# -*- coding: utf-8 -*-

import datetime
import os
import re
import threading
import time
import urllib
from xml.etree import ElementTree
import PySide.QtCore as QtCore

import DefaultSettings
import MDConst
import OsInterface as Osi


class FileHandler(object):
    def __init__(self, logger, master):
        self.logger = logger
        self.master = master
        self.rootDir = DefaultSettings.files[u'rootdir']
        self.load_settings()
        MDConst.events.files_load_settings.connect(self.load_settings)

    @QtCore.Slot()
    def load_settings(self):
        settings = self.master.master.sth.get_files_settings()
        self.rootDir = settings[u'rootdir']
        self.legalCopyFileExtensions = settings[u'copy_file_extensions']
        if self.rootDir is None:
            self.master.mh.addMessage(u'Aufnahmenverzeichnis wurde nicht gefunden.', level=MDConst.MESSAGE_ERROR,
                                      requested=False)

    def getExplorerDirContent(self, path):
        if not os.path.isdir(path):
            return []
        try:
            files = os.listdir(path)
        except Exception as e:
            self.master.mh.addMessage(u'Fehler beim Auslesen eines Ordners %s' % e, level=MDConst.MESSAGE_WARNING)
            return []
        content = []
        for i in files:
            if os.path.isdir(os.path.join(path, i)) or self.isLegalExplorerFile(os.path.join(path, i)):
                content.append(i)
        return content

    @staticmethod
    def isLegalExplorerFile(file_path):
        if Osi.WINDOWS:
            file_path = file_path.lower()
        if Osi.isHiddenPath(file_path):
            return False
        if not os.path.isfile(file_path):
            return False
        return True

    def isLegalCopyFile(self, file_path):
        if not os.path.isfile(file_path):
            return False
        for i in self.legalCopyFileExtensions:
            if file_path.endswith(i):
                return True
        return False

    def isLegalDir(self, path):
        if not os.path.isdir(path):
            return False
        if Osi.isHiddenPath(path):
            return False
        if self.rootDir and path.startswith(self.rootDir):
            return True
        for i in self.master.dh.excludedDrives:  # todo testen ob exclueded dives richtig ist
            if path.startswith(i):
                return False
        return True

    @staticmethod
    def getSpaceStr(space):
        space_dict = {0: u'B', 1: u'kB', 2: u'MB', 3: u'GB', 4: u'TB'}
        i = 0
        while space > 1024:
            space /= 1024.0
            i += 1
            if i == 5:
                break
        return u"%.1f %s" % (space, space_dict[i])

    @staticmethod
    def decodeURL(text):
        temp = urllib.unquote(text.replace('+', ' '))
        return temp.decode('utf-8')

    @staticmethod
    def correctDirName(dir_name):
        return re.sub(r'[\n\t\a\f\\\r\b\./:\*<>|\?"]', '', dir_name)  # todo adapt to microsoft specification


class RecordHandler(object):
    def __init__(self, master):
        self.master = master
        self.records = []
        self.files = []
        self.collections = []
        self.default_church = DefaultSettings.general[u'default_church']
        self.churches = []
        self.maxId = 0  # for records
        self.maxFilesId = 0
        self.maxCollectionsId = 0
        self.is_scanning = False
        self.request_scan = False
        self.run = True
        self.thread = threading.Thread(target=self.update, args=(), name=u'RecordHandlerUpdater')
        self.thread.daemon = True
        self.load_settings()
        MDConst.events.general_load_settings.connect(self.load_settings)
        # self.updateRecords()

    @QtCore.Slot()
    def load_settings(self):
        settings = self.master.master.sth.get_general_settings()
        self.default_church = settings[u'default_church']
        if self.default_church not in self.churches:
            self.churches.append(self.default_church)

    def get_default_church(self):
        return self.default_church

    def read_record_info(self, path):
        record_info = dict()
        tree = ElementTree.parse(os.path.join(path, 'record_info.xml'))
        xml_root = tree.getroot()
        # version
        record_info['version'] = int(xml_root.find('version').text)
        assert record_info['version'] >= 0
        # day
        record_info['day'] = int(xml_root.find('day').text)
        assert record_info['day'] in range(1, 32)
        # month
        record_info['month'] = int(xml_root.find('month').text)
        # year
        record_info['year'] = int(xml_root.find('year').text)
        assert record_info['year'] >= 2000
        # church
        church = xml_root.find('church').text
        if church is None:
            record_info['church'] = u''
        else:
            record_info['church'] = unicode(church)
        # event
        event = xml_root.find('event').text
        if event is None:
            record_info['event'] = u'Aufnahme'
        else:
            record_info['event'] = self.master.fh.correctDirName(unicode(xml_root.find('event').text))
        # info
        info = xml_root.find('info').text
        if info is None:
            record_info['info'] = u''
        else:
            record_info['info'] = unicode(info)
        # order
        if record_info['version'] > 0:
            record_info['order'] = int(xml_root.find('order').text)
        else:
            record_info['order'] = 0
        return record_info

    def read_collection_info(self, path):
        # todo check if file paths exists
        pass
        return dict()

    def updateRecords(self):  # todo make this more efficent
        if self.is_scanning:
            return
        self.is_scanning = True
        self.master.l.logEngine(u'scan started')
        self.master.mh.addMessage(u'Aufnahmen-Scan begonnen')
        changes = False
        new_records = self.scanRecords()
        old_records = self.records[:]

        # compare function add new to old
        for i in old_records[:]:
            is_in = False
            for j in new_records:
                if i == j:
                    is_in = True
                    break
            if not is_in:
                changes = True
                old_records.remove(i)

        for i in new_records:
            is_in = False
            for j in old_records:
                if i == j:
                    is_in = True
                    break
            if not is_in:
                changes = True
                # add id's
                temp = i
                self.maxId += 1
                temp.id = self.maxId
                old_records.append(temp)

        # scan collections
        old_collections = self.collections[:]
        new_collections = self.scanCollections()

        # todo calculate files
        old_files = self.files[:]
        if changes:
            new_files = list()
            for i in old_records:
                for j in i.files:
                    path = os.path.join(i.dir, j)
                    is_in = False
                    for k in new_files:
                        if k.path == path:
                            is_in = True
                            break
                    if not is_in:
                        new_files.append(File(None, i.day, i.month, i.year, i.church, i.event, i.info, path))
        else:
            new_files = old_files
        # add new to old files

        ''''
        # calculate size
        for i in old_records:
            size = 0
            for j in i.files:
                file = os.path.join(i.dir, j)#.decode('utf-8')
                size += os.path.getsize(file)
            i.size = size
        '''

        churches = list()
        if changes:
            for i in old_records:
                if i.church not in churches:
                    churches.append(i.church)
        else:
            churches = self.churches

        if not self.default_church in churches:
            churches.append(self.default_church)

        if changes:
            total_record_size = sum([i.size for i in old_records])
            total_collections_size = sum([i.size for i in self.collections])
            total_data_obj_size = sum([i.size for i in new_files])
            self.master.master.i.write_info('Size All Records', self.master.fh.getSpaceStr(total_record_size))
            self.master.master.i.write_info('Size All Files', self.master.fh.getSpaceStr(total_data_obj_size))

        # write results
        self.records = old_records
        self.churches = churches
        self.files = new_files

        self.master.l.logEngine(u'scan finished')
        self.master.mh.addMessage(u'Aufnahmen-Scan beendet')
        self.is_scanning = False
        self.request_scan = False

    def update(self):
        while self.run:
            try:
                self.updateRecords()
            except Exception as e:
                # traceback.print_exc()
                self.master.l.logEngine(u'Fehler beim Laden der Aufnahmen.', logLevel=MDConst.LOG_WARNING)
                self.cleanup()
            start_time = datetime.datetime.now()
            while self.run \
                    and (datetime.datetime.now() - start_time).total_seconds() < (MDConst.UPDATE_TIME_RH / 10) \
                    and not self.request_scan:
                time.sleep(1)

    def start(self):
        self.thread.start()

    def cleanup(self):
        self.is_scanning = False
        self.request_scan = False

    def scanRecords(self):
        records = list()
        if self.master.fh.rootDir is None:
            return records
        for root, dirs, files in os.walk(self.master.fh.rootDir):
            if u'record_info.xml' in files:
                try:
                    record_info = self.read_record_info(root)
                except:
                    self.master.mh.addMessage(u'Aufnahme konnte nicht ausgelesen werden: record_info.xml in %s fehlerha'
                                              u'ft.' % root, level=MDConst.MESSAGE_WARNING)  # todo testen
                    continue
                else:
                    legalFiles = list()
                    for i in files:
                        if self.master.fh.isLegalCopyFile(root + '/' + i):
                            legalFiles.append(i)
                    if not len(legalFiles) == 0:
                        records.append(Record(None, root, legalFiles, record_info['day'], record_info['month'],
                                              record_info['year'], record_info['church'], record_info['event'],
                                              record_info['info'], record_info['order']))
                        # else:
                        # print files
        return records

    def scanCollections(self):
        collections = list()
        if self.master.fh.rootDir is None:
            return collections
        colls_dir = os.path.join(self.master.fh.rootDir, MDConst.COLLECTIONSDIR)
        if os.path.isdir(colls_dir):
            for i in os.listdir(colls_dir):
                if i.lower().endswith('.xml'):
                    try:
                        coll_path = os.path.join(colls_dir, i)
                        temp_info = self.read_collection_info(coll_path)
                    except:
                        pass  # todo make sensefull error message
                    else:
                        pass  # todo
                        # temp_coll = Collection(0, temp_info['fDay'])
                        # collections.append(temp_coll)
        return collections

    def getRecordsByDate(self, date):
        record_list = []
        for i in self.records:
            if date == u'%i.%02i.%04i' % (i.day, i.month, i.year):
                record_list.append(i)
        return record_list

    def getRecordById(self, id):
        for i in self.records:
            if i.id == id:
                return i
        return None

    def getRecordsDict(self):
        records_dict = dict()
        for i in self.records:
            date = u'%d.%02d.%04d' % (i.day, i.month, i.year)
            records_dict[date] = {'id': i.id, 'event': i.event, 'size': i.size}
        return records_dict

    def getAllRecords(self):
        data = dict()
        for i in self.records:
            date = u'%i.%02i.%i' % (i.day, i.month, i.year)
            if not data.has_key(date):
                data[date] = []
            data[date].append(i)
        data_2 = dict()
        for i, j in data.iteritems():
            temp = sorted(j, key=lambda k: k.order, reverse=True)
            data_2[i] = list()
            for l in temp:
                data_2[i].append({u'id': l.id, u'name': l.event, u'size': l.size,
                                  u'church': l.church, u'date': u'%i-%02i-%02i' % (l.year, l.month, l.day)})
        return data_2
        # '''

    def getChurches(self):
        return self.churches


class DataObject(object):
    def __init__(self, data_id, day, month, year, church, event, info):
        self.id = data_id
        self.day = day
        self.month = month
        self.year = year
        self.church = church
        self.event = event
        self.info = info
        self.size = 0

    def __unicode__(self):
        return u'DataObject'


class Record(DataObject):
    def __init__(self, data_id, record_dir, files, day, month, year, church, event, info, order):
        super(self.__class__, self).__init__(data_id, day, month, year, church, event, info)
        self.dir = record_dir
        self.files = files
        self.order = order
        self.info = info
        size = 0  # todo checken ob der parameter size passt
        for i in self.files:
            file_path = os.path.join(self.dir, i)
            size += os.path.getsize(file_path)
        self.size = size

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        if not self.dir == other.dir:
            return False
        if not self.files == other.files:
            return False
        if not self.day == other.day:
            return False
        if not self.month == other.month:
            return False
        if not self.year == other.year:
            return False
        if not self.church == other.church:
            return False
        if not self.event == other.event:
            return False
        if not self.info == other.info:
            return False
        return True

    def __unicode__(self):
        return unicode(self.id) + ' ' + self.dir + ' ' + u':'.join(self.files) + \
               u' %i.%02i.%04i' % (self.day, self.month, self.year) + ' ' + self.event


class File(DataObject):
    def __init__(self, data_id, day, month, year, church, event, info, path):
        super(self.__class__, self).__init__(data_id, day, month, year, church, event, info)
        self.path = path
        self.size = os.path.getsize(self.path)
        # selfself.master = master
        # self.size = getsize...
        # self.fDay, self.fMonth, self.fYear = checkDate(fDay, fMonth, fYear)
        # self.lastDate = chechDate(lastDate)

    def __eq__(self, other):
        return self.path == other.path


class Collection(DataObject):
    def __init__(self, data_id, day, month, year, church, event, info, files_list, lDay, lMonth, lYear):
        super(self.__class__, self).__init__(data_id, day, month, year, church, event, info)

    def __eq__(self, other):
        raise


class CopyObject(object):
    def __init__(self, data):
        self.files_dict = dict()
        if isinstance(data, Record):
            pass
        elif isinstance(data, list):
            for i in data:
                if not isinstance(i, File):
                    raise  # wrong type error
        elif isinstance(data, Collection):
            pass
        else:
            raise  # wrong data type
            # calc size

    def __len__(self):
        return len(self.files_dict)


