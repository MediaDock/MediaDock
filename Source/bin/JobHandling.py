# -*- coding: utf-8 -*-

import calendar
import copy
import datetime
import os
import shutil
import threading
import time

import MDConst
from DriveHandling import Drive


class Job(object):
    def __init__(self, drive, object_list):
        # sort elements in object list by type
        # create CopyObjects from sorted elements
        # calc size form CopyObjects (for progress calculations)
        # start job
        pass


class RecordJob(object):
    def __init__(self, master, record_job_id, drive_id, records_id_list):
        self.master = master
        self.id = record_job_id
        self.drive = master.master.dh.getDriveById(drive_id)
        self.progress = 0
        self.buf_size = 16 * 1024
        self.nr_of_loops = 0
        self.start_time = datetime.datetime.now()
        self.left_time = u'wird berechnet'
        self.last_times = list()
        self.calc_time_cycle = 10
        self.recordList = list()
        for i in records_id_list:
            j = master.master.rh.getRecordById(i)
            if j:
                self.recordList.append(copy.deepcopy(j))
            else:
                self.master.master.mh.addMessage(u'Fehler im Aufsführen eines Aufnahmeauftrages: Aufnahme fehlt',
                                                 level=MDConst.MESSAGE_WARNING, requested=False)
        self.number_of_files = 0
        for i in self.recordList:
            self.number_of_files += len(i.files)
        if self.number_of_files == 0:
            master.master.mh.addMessage(u'Auftrag hat keine Dateien', level=MDConst.MESSAGE_WARNING, requested=False)
            return
        space = 0
        for i in self.recordList:
            space += i.size
        self.size = space
        self.expected_loops = int(self.size / self.buf_size)
        self.drive.leftSpace = self.drive.leftSpace - space
        if self.drive.leftSpace < 0:
            self.drive.leftSpace = 0
        if self.drive.requestChangeState(MDConst.DRIVE.RECORD_JOB_RUNS):
            self.run = True
            thread_name = u'RecordJob%i' % self.id
            self.thread = threading.Thread(target=self.copyFiles, args=(), name=thread_name)
            self.thread.start()
        else:
            self.master.master.mh.addMessage(u'Fehler im Aufsführen eines Aufnahmeauftrages: Laufwerk %s anderweitig '
                                             u'beschäftigt' % unicode(self.drive), level=MDConst.MESSAGE_WARNING,
                                             requested=False)

    def __str__(self):
        return 'RecordJob ID: %i, Target: %s, Progress: %i of %i' % (self.id, str(self.drive), self.progress,
                                                                     self.number_of_files)

    def __unicode__(self):
        return u'RecordJob ID: %i, Target: %s, Progress: %i of %i' % (self.id, unicode(self.drive), self.progress,
                                                                      self.number_of_files)

    def getProgress(self):
        return self.progress, self.number_of_files

    def getProgressSizePercent(self):
        if self.size > 0:
            progress = (self.nr_of_loops * self.buf_size * 100) / self.size
            if progress >= 99:
                return 99
            else:
                return progress
        return 0

    def calculateLeftTime(self):

        if self.nr_of_loops < 1000:
            self.left_time = u'wird berechnet'
            return

        run_time = datetime.datetime.now() - self.start_time
        time_per_loop = run_time.total_seconds() / self.nr_of_loops
        # print 'time_per_loop', time_per_loop, self.nr_of_loops, run_time.total_seconds()
        left_loops = self.expected_loops - self.nr_of_loops
        if left_loops <= 0:
            self.left_time = u'wird berechnet'
            return
        left_time = left_loops * time_per_loop

        self.left_time = u'wird berechnet'

        if left_time >= 3600:  # > 1h
            temp = divmod(left_time, 3600)
            self.left_time = u'>%i h' % int(temp[0])
        elif left_time >= 600:  # 1h - 15min / 15min
            temp = divmod(left_time, 900)
            self.left_time = u'%i min' % int((temp[0] + 1) * 15)
        elif left_time >= 60:  # 15min - 1min / 1min
            temp = divmod(left_time, 60)
            self.left_time = u'%i min' % int(temp[0] + 1)
        elif left_time >= 15:  # 1min - 15s / 15s
            temp = divmod(left_time, 15)
            self.left_time = u'%i s' % int((temp[0] + 1) * 15)
        elif left_time >= 1:  # 15s - 1s / 1s
            self.left_time = u'%i s' % int(left_time + 1)
        else:  # < 1s
            self.left_time = u'gleich fertig'

    def getLeftTime(self):
        return self.left_time

    def cancel(self):
        self.run = False

    def copyFiles(self):
        number_of_errors = 0
        self.master.master.mh.addMessage(u'Auftrag gestartet für Laufwerk ' + unicode(self.drive),
                                         level=MDConst.MESSAGE_INFO)
        for i in self.recordList:
            self.master.master.stat.copyRecord(i.dir)
            if not self.run:
                break
            dir_name = self.master.makeRecordDirName(self.drive.mountpoint, i)
            if os.path.isdir(dir_name):
                self.master.master.mh.addMessage(u'Fehler im ausführen eines Kopierauftrages: Ordner mit dem Namen %s '
                                                 u'ist bereits vorhanden' % dir_name, level=MDConst.MESSAGE_ERROR,
                                                 requested=False)
                self.number_of_files -= len(i.files)
                number_of_errors += len(i.files)
                continue
            os.mkdir(dir_name)
            self.master.master.l.logCopy(u'Start copy record -> Event: %s Date: %s, %d %d; RecordID: %d; RecordJobID '
                                         u':%d; Drive: (%s)' %
                                         (i.event, calendar.month_name[i.month], i.day, i.year, i.id, self.id,
                                          self.drive.get_info_unicode()), logLevel=MDConst.LOG_INFO)
            for j in i.files:
                if not self.run:
                    break
                source_file = os.path.join(i.dir, j)
                try:
                    self.master.master.l.logCopy(u'Start copy file: %s, RecordID: %d, RecordJobID: %d' %
                                                 (source_file, i.id, self.id), logLevel=MDConst.LOG_INFO)

                    target_file_name = os.path.join(dir_name, os.path.basename(source_file))
                    src_file = open(source_file, 'rb')
                    target_file = open(target_file_name, 'wb')
                    not_fat_file_system = not self.drive.file_system.lower().startswith('fat')
                    while self.run:
                        buf = src_file.read(self.buf_size)
                        if not buf:
                            break
                        target_file.write(buf)
                        target_file.flush()
                        if not_fat_file_system:
                            os.fsync(target_file.fileno())
                        self.nr_of_loops += 1
                        if not self.nr_of_loops % self.calc_time_cycle:
                            self.calculateLeftTime()
                    src_file.close()
                    target_file.close()
                    if not self.run:
                        os.remove(target_file_name)
                    else:
                        shutil.copystat(source_file, target_file_name)
                        self.master.master.stat.copyFile(source_file)
                        # '''

                except Exception as e:
                    # print traceback.format_exc()
                    self.master.master.mh.addMessage(u'Fehler beim kopieren einer Datei nach %s Fehler: %s' %
                                                     (dir_name, unicode(e)), level=MDConst.MESSAGE_ERROR)
                    self.master.master.l.logCopy(u'Error in copy file: %s, RecordID: %d, RecordJobID: %d' %
                                                 (source_file, i.id, self.id), logLevel=MDConst.LOG_ERROR, )
                    self.number_of_files -= 1
                    number_of_errors += 1
                else:
                    self.progress += 1
                    self.master.master.l.logCopy(u'Finished copy file: %s, RecordID: %d, RecordJobID: %d' %
                                                 (source_file, i.id, self.id), logLevel=MDConst.LOG_INFO)
            self.master.master.l.logCopy(u'Finished copy record ->  RecordID: %d, RecordJobID: %d' %
                                         (i.id, self.id), logLevel=MDConst.LOG_INFO)
        if self.drive.requestChangeState(MDConst.DRIVE.IDLE):
            if self.run:
                if number_of_errors == 0:
                    self.master.master.mh.addMessage(u'Auftrag erfolgreich abgeschlossen für Laufwerk ' +
                                                     unicode(self.drive), requested=False, level=MDConst.MESSAGE_INFO)
                else:
                    self.master.master.mh.addMessage(u'Auftrag abgeschlossen für Laufwerk %s mit %i Fehlern' %
                                                     (unicode(self.drive), number_of_errors), requested=False,
                                                     level=MDConst.MESSAGE_ERROR)
            else:
                if number_of_errors == 0:
                    self.master.master.mh.addMessage(u'Auftrag für Laufwerk ' + unicode(self.drive) + ' abgebrochen.',
                                                     requested=False, level=MDConst.MESSAGE_INFO)
                else:
                    self.master.master.mh.addMessage(u'Auftrag für Laufwerk %s abgebrochen mit %i Fehlern' %
                                                     (unicode(self.drive), number_of_errors), requested=False,
                                                     level=MDConst.MESSAGE_ERROR)
        else:
            self.drive.requestChangeState(MDConst.DRIVE.ERROR, True)
            self.master.master.mh.addMessage(u'Fehler im Status des Laufwerks ' + unicode(self.drive), requested=False,
                                             level=MDConst.MESSAGE_WARNING)
        self.master.jobs.remove(self)
        try:  # todo alle messages testen
            self.drive.updateSpace()
        except:
            self.master.master.mh.addMessage(u'Fehler beim Auslesen der Datenträgergröße ' + unicode(self.drive),
                                             level=MDConst.MESSAGE_WARNING)
            # print 'needed time', datetime.datetime.now() - self.start_time
            # print self.nr_of_loops


class JobHandler(object):
    maxId = 0

    def __init__(self, master):
        self.master = master
        self.maxJobsOnTime = 10
        self.jobs = []

    def __str__(self):
        return '; '.join(str(x) for x in self.jobs)

    def __unicode__(self):
        return '; '.join(unicode(x) for x in self.jobs)

    def makeRecordDirName(self, mount_point, record):
        dir_name = u'%i_%02i_%02i_[%02i]_%s_%s' % (record.year, record.month, record.day, 10 - record.order,
                                                   self.master.fh.correctDirName(record.event),
                                                   self.master.fh.correctDirName(record.church))
        return os.path.join(mount_point, dir_name)

    def addRecordJob(self, drive_id, records_id_list):
        check_result = self.checkRecordJob(drive_id, records_id_list)
        if check_result[0]:
            self.jobs.append(RecordJob(self, JobHandler.maxId, drive_id, records_id_list))
            JobHandler.maxId += 1
        else:
            self.master.mh.addMessage(u'Auftrag konnte nicht gestartet werden: %s' % check_result[1],
                                      level=MDConst.MESSAGE_WARNING, requested=False)
            # todo testen

    def checkRecordJob(self, drive_id, records_id_list):
        result = self.master.master.th.check_job_ready()
        if not result[0]:
            return result
        if self.numberOfJobs() >= self.maxJobsOnTime:
            return False, u"Maximale Anzahl von Aufträgen erreicht"
        record = self.getRecordJobByDriveId(drive_id)
        if record:
            return False, u'Es läuft schon ein Auftrag für dieses Laufwerk'
        drive = self.master.dh.getDriveById(drive_id)
        if not isinstance(drive, Drive):
            return False, u'Laufwerk nicht vorhanden'
        if not drive.state == MDConst.DRIVE.IDLE:
            return False, u'Laufwerk anderweitig beschäfigt'
        records_list = [self.master.rh.getRecordById(i) for i in records_id_list]
        if len(records_list) == 0:
            return False, u'Es wurden keine Aufnahmen angegeben'
        if None in records_list:
            return False, u'Ausgewählte Aufnahme nicht vorhanden'
        total_space = 0
        dirs = list()
        for i in records_list:
            total_space += i.size
            dir_name = self.makeRecordDirName('', i)
            dirs.append(dir_name)
        dirs_on_drive = os.listdir(unicode(drive.mountpoint))
        problem_dirs = list()
        for i in dirs_on_drive:
            if i in dirs:
                problem_dirs.append(i)
        if problem_dirs:
            return False, u"Es gibt schon Ordner mit dem/n Namen auf dem Laufwerk: " + u', '.join(problem_dirs)
        double_dirs = list()
        for i in dirs:
            if dirs.count(i) > 1 and i not in double_dirs:
                double_dirs.append(i)
        if double_dirs:
            return False, u"Folgende Aufnahmen wurden doppelt ausgewählt: " + u", ".join(double_dirs)
        if total_space > drive.leftSpace:
            return False, u'Es ist nicht genug Speicherplatz auf dem Speichermedium vorhanden. Es fehlen %s' \
                   % self.master.fh.getSpaceStr(total_space - drive.leftSpace)
        return True, u'Job ist in Ordnung'

    def addJob(self):
        pass

    def checkJob(self):
        pass

    def getRecordJobById(self):
        for i in self.jobs:
            if isinstance(i, RecordJob) and i.id == id:
                return i
        return None

    def getRecordJobByDriveId(self, drive_id):
        for i in self.jobs:
            if isinstance(i, RecordJob) and i.drive.id == drive_id:
                return i
        return None

    def numberOfJobs(self):
        return len(self.jobs)

    def numberOfRunningJobs(self):
        number = 0
        for i in self.jobs:
            if i.state in (MDConst.DRIVE.RECORD_JOB_RUNS, MDConst.DRIVE.FILE_JOB_RUNS):
                number += 1
        return number

    def hasJobsForDrive(self, drive_id):
        drive = self.master.dh.getDriveById(drive_id)
        for i in self.jobs:
            if i.drive == drive:
                return True
        return False

    def deleteJob(self, job):
        job.stop()
        while job.isRunning():
            time.sleep(0.01)
        self.jobs.remove(job)

    def deleteAllJobs(self):
        for i in self.jobs:
            self.deleteJob(i)

    def getJobsForDrive(self, drive_id):
        jobs = list()
        for i in self.jobs:
            if i.drive.id == drive_id:
                jobs.append(i)
        return jobs

    def forceDeleteJobsForDrive(self, drive_id):
        for i in self.jobs:
            if i.drive.id == drive_id:
                i.cancel()
                self.jobs.remove(i)

    def update(self):
        self.master.master.i.write_info(u'Jobs', unicode(self))
        for i in self.jobs:
            if not i.thread.is_alive():
                i.cancel()
                self.jobs.remove(i)
