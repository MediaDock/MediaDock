# -*- coding: utf-8 -*-

import os
import threading
import traceback
import PySide.QtCore as QtCore

import MDConst
import OsInterface as Osi


class Drive(object):
    def __init__(self, master, device):
        self.master = master
        self.device = device
        self.id = DriveHandler.maxId
        self.__state = MDConst.DRIVE.IDLE
        self.deleting = False
        self.deleteThread = None
        DriveHandler.maxId += 1
        volume_info = Osi.getVolumeInfo(device)
        self.mountpoint = volume_info[0]
        self.name = volume_info[1]
        self.totalSpace = volume_info[2]
        self.freeSpace = volume_info[3]
        self.leftSpace = volume_info[3]
        self.file_system = volume_info[4]

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name.encode('utf-8')

    def get_info_str(self):
        return 'Drive ' \
               'id: %s, ' \
               'mountpoint: %s, ' \
               'device: %s, ' \
               'name: %s, ' \
               'state: %i, ' \
               'total_space: %i, ' \
               'free_space: %i, ' \
               'left_space: %i, ' \
               'file_system: %s' % (
                   self.id,
                   self.mountpoint,
                   self.device,
                   str(self),
                   self.__state,
                   self.totalSpace,
                   self.freeSpace,
                   self.leftSpace,
                   self.file_system
               )

    def get_info_unicode(self):
        return 'Drive ' \
               'id: %s, ' \
               'mountpoint: %s, ' \
               'device: %s, ' \
               'name: %s, ' \
               'state: %i, ' \
               'total_space: %i, ' \
               'free_space: %i, ' \
               'left_space: %i, ' \
               'file_system: %s' % (
                   self.id,
                   self.mountpoint,
                   self.device,
                   unicode(self),
                   self.__state,
                   self.totalSpace,
                   self.freeSpace,
                   self.leftSpace,
                   self.file_system
               )

    @property
    def state(self):
        return self.__state

    def requestChangeState(self, target_state, force=False):
        if force:
            self.__state = target_state
            return True
        elif self.__state == MDConst.DRIVE.IDLE and target_state in (MDConst.DRIVE.IDLE,
                                                                     MDConst.DRIVE.RECORD_JOB_RUNS,
                                                                     MDConst.DRIVE.FILE_JOB_RUNS,
                                                                     MDConst.DRIVE.DELETES,
                                                                     MDConst.DRIVE.EJECTING):
            self.__state = target_state
            return True
        elif self.__state == MDConst.DRIVE.RECORD_JOB_RUNS and target_state in (MDConst.DRIVE.IDLE,
                                                                                MDConst.DRIVE.ERROR):
            self.__state = target_state
            return True
        elif self.__state == MDConst.DRIVE.FILE_JOB_RUNS and target_state in (MDConst.DRIVE.IDLE,
                                                                              MDConst.DRIVE.ERROR):
            self.__state = target_state
            return True
        elif self.__state == MDConst.DRIVE.DELETES and target_state in (MDConst.DRIVE.IDLE,
                                                                        MDConst.DRIVE.ERROR):
            self.__state = target_state
            return True
        elif self.__state == MDConst.DRIVE.EJECTING and target_state in (MDConst.DRIVE.IDLE,):
            self.__state = target_state
            return True
        elif self.__state == MDConst.DRIVE.ERROR and target_state in (MDConst.DRIVE.EJECTING,):
            self.__state = target_state
            return True
        return False

    def eject(self):
        success = Osi.ejectDrive(self.device, self.master.master.mh)
        if success:
            self.master.drives.remove(self)
        return success

    def getUsage(self):
        if self.totalSpace == 0:
            return '0%'
        return '%i%%' % (((self.totalSpace - self.freeSpace) * 100) / self.totalSpace)

    def delete_dir(self, directory, errors):
        try:
            dir_list = os.listdir(directory)
        except OSError:
            self.master.master.mh.addMessage(u'Fehler beim Löschen: Pfad konnte nicht gefunden werden: ' + directory,
                                             level=MDConst.MESSAGE_ERROR)  # todo testen
            return 1
        for i in dir_list:
            if not self.deleting:
                break
            path = os.path.join(directory, i)
            if os.path.isdir(path):
                if not Osi.isHiddenPath(path):
                    errors += self.delete_dir(path, errors)
                    try:
                        os.rmdir(path)
                    except OSError:
                        errors += 1
                        self.master.master.mh.addMessage(u'Fehler beim Löschen des Ordners: ' + path,
                                                         level=MDConst.MESSAGE_ERROR)  # todo testen
            if os.path.isfile(path):
                try:
                    os.remove(path)
                except OSError:
                    errors += 1
                    self.master.master.mh.addMessage(u'Fehler beim Löschen der Datei: ' + path,
                                                     level=3)  # todo testen
        return errors

    def delete(self):
        result = self.master.checkDelete(self.id)
        if not result[0]:
            self.deleteThread = None
            return
        if self.requestChangeState(MDConst.DRIVE.DELETES):
            self.deleting = True
            self.master.master.mh.addMessage(u'Löschen gestartet für Laufwerk: ' + unicode(self),
                                             level=MDConst.MESSAGE_INFO)
            errors = self.delete_dir(unicode(self.mountpoint), 0)
            if self.deleting:
                if errors:
                    if errors > 1:
                        self.master.master.mh.addMessage(u'Löschen mit %i Fehlern abgeschlossen.' % errors,
                                                         level=MDConst.MESSAGE_ERROR, requested=False)
                    else:
                        self.master.master.mh.addMessage(u'Löschen mit einem Fehler abgeschlossen.',
                                                         level=MDConst.MESSAGE_ERROR, requested=False)
                else:
                    self.master.master.mh.addMessage(u'Löschen erfogreich abgeschlossen.', level=MDConst.MESSAGE_INFO,
                                                     requested=False)
            else:
                if errors:
                    if errors > 1:
                        self.master.master.mh.addMessage(u'Löschen mit % Fehlern abgebrochen.' % errors,
                                                         level=MDConst.MESSAGE_ERROR, requested=False)
                    else:
                        self.master.master.mh.addMessage(u'Löschen mit einem Fehler abgebrochen.',
                                                         level=MDConst.MESSAGE_ERROR, requested=False)
                else:
                    self.master.master.mh.addMessage(u'Löschen abgebrochen.', level=MDConst.MESSAGE_INFO,
                                                     requested=False)
            if self.requestChangeState(MDConst.DRIVE.IDLE):
                # noinspection PyBroadException
                try:
                    self.leftSpace = Osi.getVolumeFreeSpace(self.device)
                    self.freeSpace = Osi.getVolumeFreeSpace(self.device)
                except:
                    self.leftSpace = 0
                    self.freeSpace = 0
                    self.master.master.mh.addMessage(u'Laufwerksinformationen konnten nicht ausgelesen werden für: ' +
                                                     unicode(self), level=MDConst.MESSAGE_WARNING)
            else:
                self.requestChangeState(MDConst.DRIVE.ERROR, True)
                self.master.l.logEngine(u'Error: Laufwerk %s  hat den Status während des Löschens geändert' %
                                        unicode(self), logLevel=MDConst.LOG_ERROR)
                self.master.mh.addMessage(u'Error: Laufwerk %s  hat den Status während des Löschens geändert' %
                                          unicode(self), level=MDConst.MESSAGE_ERROR, requested=False)
            self.deleting = False
        self.deleteThread = None

        """
        if self.state == 0:
            self.state = 3

            # deleting

            if self.state == 3:
                try:
                    self.leftSpace = Osi.getVolumeFreeSpace(self.device)
                    self.freeSpace = Osi.getVolumeFreeSpace(self.device)
                except:
                    self.leftSpace = 0
                    self.freeSpace = 0
                    self.master.master.mh.addMessage(u'Laufwerksinformationen konnten nicht ausgelesen werden für: ' +
                                                     unicode(self), level=MDConst.MESSAGE_WARNING)
                self.state = 0
            else:
                self.state = -1
                self.master.l.logEngine(u'Error: Laufwerk %s  hat den Status während des Löschens geändert' %
                                        unicode(self), logLevel=MDConst.LOG_ERROR)
                self.master.mh.addMessage(u'Error: Laufwerk %s  hat den Status während des Löschens geändert' %
                                          unicode(self), level=MDConst.MESSAGE_ERROR, requested=False)
            self.deleting = False
        self.deleteThread = None
        """

    def start_delete_thread(self):
        self.deleteThread = threading.Thread(target=self.delete, args=(), name=u'DeleteThreadDrive' + unicode(self.id))
        self.deleteThread.start()

    def stop_deleting(self):
        self.deleting = False

    def updateSpace(self):
        if self.master.master.jh.hasJobsForDrive(self.id):
            return
        volume_info = Osi.getVolumeInfo(self.device)
        self.totalSpace = volume_info[2]
        self.freeSpace = volume_info[3]
        self.leftSpace = volume_info[3]


class DriveHandler(object):
    maxId = 0

    def __init__(self, master):
        self.master = master
        self.drives = []
        self.excludedDrives = []
        self.useExclusionList = True
        self.removableDrivesOnly = True
        self.load_settings()
        MDConst.events.general_load_settings.connect(self.load_settings)

    def __str__(self):
        return unicode(self).encode('utf-8')

    def __unicode__(self):
        return ', '.join(unicode(x) + u' ' + MDConst.DRIVE.STATE_DICT[x.state] + u' ' + x.file_system
                         for x in self.drives)

    @QtCore.Slot()
    def load_settings(self):
        settings = self.master.master.sth.get_general_settings()
        self.excludedDrives = settings[u'drives_excluded_drives']
        self.useExclusionList = settings[u'drives_use_exclusion_list']
        self.removableDrivesOnly = settings[u'drives_removable_drives_only']

    def update(self):
        drives = Osi.getVolumesList(self.removableDrivesOnly, self.useExclusionList, self.excludedDrives)

        # check if there are new drives
        temp_drives = drives[:]
        temp_drives2 = drives[:]
        for i in temp_drives:
            for j in self.drives:
                if i == j.device:
                    temp_drives2.remove(i)
        for i in temp_drives2:
            try:
                Osi.getVolumeName(i)
            except Exception as e:
                self.master.l.logEngine(u'Fehler beim Auslesen des Laufwerks %s' % i, logLevel=MDConst.LOG_WARNING)
                if not MDConst.FROZEN:
                    print e
                    print traceback.format_exc()
            else:
                new_drive = Drive(self, i)
                self.drives.append(new_drive)
                self.master.mh.addMessage(u'Neues Laufwerk erkannt: %s' % new_drive, level=MDConst.MESSAGE_INFO,
                                          sections=[0, 1])

        # check if there are missing drives
        temp_drives = self.drives[:]
        temp_drives2 = self.drives[:]
        drives = Osi.getVolumesList(self.removableDrivesOnly, self.useExclusionList, self.excludedDrives)
        for i in temp_drives:
            for j in drives:
                if i.device == j:
                    temp_drives2.remove(i)
        for i in temp_drives2:
            self.master.mh.addMessage(u'Laufwerk %s wurde unzulässig entfehrnt' % i.name, level=MDConst.MESSAGE_ERROR,
                                      sections=[0, 1], requested=False)
            self.master.jh.forceDeleteJobsForDrive(i.id)
            self.drives.remove(i)

        # calculate free space
        for i in self.drives:
            # noinspection PyBroadException
            try:
                i.freeSpace = Osi.getVolumeFreeSpace(i.device)
            except:
                self.master.mh.addMessage(u'Informationen über Laufwerk %s konnten nicht ausgelesen werden' % i,
                                          level=MDConst.MESSAGE_WARNING, sections=[0, 1], lifespan=2000)

        # check
        # for i in self.drives:
        #     if not self.master.jh.hasJobsForDrive(i.id) and i.state == 1:
        #         i.state = 0
        #         self.master.mh.addMessage(u'Zustand von %s auf idle gesetzt' % i, level=MDConst.MESSAGE_INFO)
        #         self.master.mh.addMessage(u'Unbekannter Fehler im Laufwerk %s' % i, level=MDConst.MESSAGE_WARNING,
        #                                  requested=False)

        # self.master.master.i.write_info('Drives', ', '.join(str(x) + x.state for x in self.drives))
        self.master.master.i.write_info(u'Drives', unicode(self))

    def hasDrive(self, device):
        for i in self.drives:
            if i.device == device:
                return True
        return False

    def ejectDrive(self, drive_id):
        for i in self.drives:
            if str(i.id) == drive_id:
                if i.requestChangeState(MDConst.DRIVE.EJECTING):
                    name = i.name
                    self.master.update_drive_pause += 1
                    # noinspection PyBroadException
                    try:
                        temp = i.eject()
                    except:
                        self.master.mh.addMessage(u'Fehler beim Entfernen es Laufwerk %s' % name,
                                                  level=MDConst.MESSAGE_ERROR, sections=[0, 1], requested=False)
                        i.requestChangeState(MDConst.DRIVE.ERROR, True)
                    else:
                        if temp:
                            self.master.mh.addMessage(u'Laufwerk %s erfogreich entfernt' % name,
                                                      level=MDConst.MESSAGE_INFO, sections=[0, 1], requested=False)
                        else:
                            self.master.mh.addMessage(u'Laufwerk %s konnte nicht entfernt werden' % name,
                                                      level=MDConst.MESSAGE_WARNING, sections=[0, 1], requested=False)
                            i.requestChangeState(MDConst.DRIVE.IDLE)
                        break
                    finally:
                        self.master.update_drive_pause -= 1
                else:
                    self.master.mh.addMessage(
                        u'Laufwerk %s konnte nicht entfernt werden, es ist anderweitig beschäfitgt.'
                        % i.name, level=MDConst.MESSAGE_INFO, sections=[0, 1], requested=False)

    def getMountPoints(self):
        mount_point_list = []
        for drive in self.drives:
            mount_point_list.append(drive.mountpoint)
        return mount_point_list

    def getMountPointById(self, mount_point_id):
        for i in self.drives:
            if i.id == mount_point_id:
                return i.mountpoint
        return ''

    def getDriveById(self, drive_id):
        for i in self.drives:
            if i.id == drive_id:
                return i
        return None

    def checkDelete(self, drive_id):
        drive = self.getDriveById(drive_id)
        if not isinstance(drive, Drive):
            return False, u'Fehler in der Laufwerks-ID'
        if not drive.state == MDConst.DRIVE.IDLE:
            return False, u'Laufwerk ist anderweitig beschäftigt'
        result = self.master.master.th.check_job_ready()
        if not result[0]:
            return result
        return True, u'Laufwerk bereit zum Löschen'  # todo testen

    def has_delete_tasks(self):
        for i in self.drives:
            if i.deleteThread is not None:
                return True
        return False
