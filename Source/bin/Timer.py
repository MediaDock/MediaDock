# coding=utf-8

import time
from datetime import datetime, timedelta
import threading

import MDConst
import DefaultSettings
import PySide.QtCore as QtCore


class TimeHandler(object):
    state_dict = {0: u'start', 1: u'run', 2: u'wait_for_engine_stop_timer', 3: u'wait_for_engine_stop_hand',
                  4: u'askshutdown_timer', 5: u'run_timer', 6: u'askshutdown_start', 7: u'shutdown',
                  8: u'wait_engine_stop_quit', 9: u'quit', 10: u'end'}

    def __init__(self, master):
        self.master = master
        self.settings = DefaultSettings.timer
        self.is_locked = False
        self.manual_lock = False
        self.init_scan_done = False
        self.run_day = True
        self.auto_shutdown = True
        self.master.i.write_info(u'auto_shutdown', self.auto_shutdown)
        self.thread = threading.Thread(target=self.update, name=u'TimeHandlerUpdate')
        self.thread.daemon = True
        # self.check_shutdown_thread = threading.Thread(target=self.check_shutdown, name='CheckShutdownThread')
        # self.check_shutdown_thread.daemon = True
        self.state = 0
        self.switch_timer = None
        self.load_timer_settings()
        MDConst.events.timer_load_settings.connect(self.load_timer_settings)

    def set_auto_shutdown(self):
        self.auto_shutdown = True
        self.master.i.write_info(u'auto_shutdown', self.auto_shutdown)

    def unset_auto_shutdown(self):
        self.auto_shutdown = False
        self.master.i.write_info(u'auto_shutdown', self.auto_shutdown)

    def start(self):
        self.load_timer_settings()
        self.thread.start()

    def init_scan(self):
        self.init_scan_done = True
        locked = False
        now = datetime.now()
        now_day = unicode(now.weekday())
        now_date = u'%02i.%02i' % (now.day, now.month)
        now_time = u'%02i:%02i' % (now.hour, now.minute)
        loop_break = False

        if now_date in self.settings[u'lc_time_yearly'] or now_date in self.settings[u'ul_time_yearly']:
            for i in range(24):
                for j in range(60):
                    test_time = u'%02i:%02i' % (i, j)
                    if test_time in self.settings[u'lc_time_yearly'][now_date]:
                        locked = True
                    if test_time in self.settings[u'ul_time_yearly'][now_date]:
                        locked = False
                    if test_time == now_time:
                        loop_break = True
                    if loop_break:
                        break
                if loop_break:
                    break

            return locked

        else:
            if now_day not in self.settings[u'lc_time_weekly']:
                return False

            for i in range(24):
                for j in range(60):
                    test_time = u'%02i:%02i' % (i, j)
                    if test_time in self.settings[u'lc_time_weekly'][now_day]:
                        locked = True
                    if now_day in self.settings[u'ul_time_weekly'] and \
                                    test_time in self.settings[u'ul_time_weekly'][now_day]:
                        locked = False
                    if test_time == now_time:
                        loop_break = True
                    if loop_break:
                        break
                if loop_break:
                    break

        return locked

    @QtCore.Slot()
    def load_timer_settings(self):
        self.settings = self.master.sth.get_timer_settings()

        self.master.i.write_info(u'sd_time_weekly', str(self.settings[u'sd_time_weekly']))
        self.master.i.write_info(u'sd_time_yearly', str(self.settings[u'sd_time_yearly']))
        self.master.i.write_info(u'lc_time_weekly', str(self.settings[u'lc_time_weekly']))
        self.master.i.write_info(u'lc_time_yearly', str(self.settings[u'lc_time_yearly']))
        self.master.i.write_info(u'ul_time_weekly', str(self.settings[u'ul_time_weekly']))
        self.master.i.write_info(u'ul_time_yearly', str(self.settings[u'ul_time_yearly']))
        self.master.i.write_info(u'nr_days', str(self.settings[u'nr_days']))
        self.master.i.write_info(u'time_transition_4_7', self.settings[u'time_transition_4_7'])
        self.master.i.write_info(u'time_transition_5_2', self.settings[u'time_transition_5_2'])
        self.master.i.write_info(u'time_transition_6_7', self.settings[u'time_transition_6_7'])

    def reload_timer(self):
        self.load_timer_settings()
        if not self.manual_lock:
            self.is_locked = self.init_scan

    def lock_manually(self):
        self.is_locked = True
        self.manual_lock = True

    def unlock(self):
        self.is_locked = False
        self.manual_lock = False

    def shutdown(self):
        self.master.l.logMediaDock(u'Shutdown by timer', logLevel=MDConst.LOG_INFO)
        self.master.start_shutdown_thread()

    def update_fsm(self):
        if self.state == 0:
            if datetime.now().weekday() in self.settings[u'nr_days']:
                self.state = 6
                self.switch_timer = datetime.now()
            else:
                self.state = 1
        elif self.state == 1:
            if self.check_timer_shutdown():
                self.state = 2
        elif self.state == 2:
            if self.master.e.is_stop_ready():
                self.state = 4
                self.switch_timer = datetime.now()
        elif self.state == 3:
            if not self.auto_shutdown:
                self.state = 5
                self.switch_timer = datetime.now()
            if self.master.e.is_stop_ready():
                self.state = 7
        elif self.state == 4:
            if not self.auto_shutdown:
                self.state = 5
                self.switch_timer = datetime.now()
            if (datetime.now() - self.switch_timer) > timedelta(minutes=self.settings[u'time_transition_4_7']):
                self.state = 7
        elif self.state == 5:
            if self.auto_shutdown and (datetime.now() - self.switch_timer) > timedelta(
                    minutes=self.settings[u'time_transition_5_2']):
                self.state = 2
        elif self.state == 6:
            if (datetime.now() - self.switch_timer) > timedelta(minutes=self.settings[u'time_transition_6_7']):
                self.state = 7
        elif self.state == 7:
            self.master.l.logMediaDock(u'Shutdown by FSM Timer', logLevel=MDConst.LOG_INFO)
            self.master.shutdown = True
            self.master.b.stop()
            self.state = 10
        elif self.state == 8:
            if self.master.e.is_stop_ready():
                self.state = 9
        elif self.state == 9:
            self.master.l.logMediaDock(u'Quit by FSM Timer', logLevel=MDConst.LOG_INFO)
            self.master.b.stop()
            self.state = 10
        elif self.state == 10:
            try:
                self.master.b.stop()
            except:
                pass
        else:
            self.master.mh.addMessage(u'Fehler in FSM Timer: Unbekannter Zustand. Zustand auf run timer gesetzt',
                                      level=MDConst.MESSAGE_WARNING)
            self.master.l.logMediaDock(u'Error in FSM Timer: Unknown state. State set to run timer',
                                       logLevel=MDConst.MESSAGE_ERROR)
            self.state = 5
            self.switch_timer = datetime.now()
        self.master.i.write_info(u'timer_fsm_state', TimeHandler.state_dict[self.state])

    def get_left_time(self):
        if self.state in (6, 4):
            time_delta = datetime.now() - self.switch_timer
            if self.state == 6:
                time_delta = timedelta(minutes=self.settings[u'time_transition_6_7']) - time_delta
            elif self.state == 4:
                time_delta = timedelta(minutes=self.settings[u'time_transition_4_7']) - time_delta
            time_delta = time_delta.total_seconds()
            time_delta = divmod(time_delta, 60)
            if time_delta[0] == 0:
                if time_delta[1] < 30:
                    return u'<30s'
                else:
                    return u'<1min'
            else:
                return u'%imin' % time_delta[0]
        else:
            return u'---'

    def request_quit(self):
        if self.state in (1, 2, 3, 4, 5, 6):
            self.state = 8

    def request_shutdown(self):
        if self.state in (1, 2, 4, 5, 6, 8):
            self.state = 3

    def set_run_timer(self):
        if self.state in (3, 4, 8):
            self.state = 5
            self.switch_timer = datetime.now()

    def cancel_sd_start(self):
        if self.state == 6:
            self.state = 1

    def set_run(self):
        if self.state == 5:
            self.state = 1

    def check_job_ready(self):
        if self.state in (1, 5):
            return True, u'Auftrag kann gestartet werden.'
        if self.state == 0:
            return False, u'Das Programm wird gestartet.'
        if self.state == 2:
            return False, u'Warten Sie bis alle Aufträge beendet sind und brec' \
                          u'hen sie dann das Herunterfahren ab.'
        if self.state == 3:
            return False, u'Brechen Sie das Herunterfahren ab.'
        if self.state == 4:
            return False, u'Brechen Sie das Herunterfahren ab.'
        if self.state == 6:
            return False, u'Brechen Sie das Herunterfahren ab.'
        if self.state == 7:
            return False, u'Das Programm wird gleich beendet.'
        if self.state == 8:
            return False, u'Brechen Sie das Herunterfahren ab.'
        if self.state == 9:
            return False, u'Das Programm wird gleich beendet.'
        if self.state == 10:
            return False, u'Das Programm wird gleich beendet.'
        return False, u'Es kann kein Auftrag gestartet werden.'

    def update(self):
        self.lock = self.init_scan

        while True:
            self.update_lock()

            old_state = self.state
            self.update_fsm()
            while old_state != self.state:
                old_state = self.state
                self.update_fsm()

            time.sleep(1)

    def check_timer_shutdown(self):
        now = datetime.now()
        now_date = u'%02i.%02i' % (now.day, now.month)
        now_time = u'%02i:%02i' % (now.hour, now.minute)
        now_day = unicode(now.weekday())
        if now_date in self.settings[u'sd_time_yearly']:
            if now_time in self.settings[u'sd_time_yearly'][now_date]:
                return True
        elif now_day in self.settings[u'sd_time_weekly']:
            if now_time in self.settings[u'sd_time_weekly'][now_day]:
                return True
        return False

    def update_lock(self):

        # check shutdown
        now = datetime.now()
        now_date = u'%02i.%02i' % (now.day, now.month)
        now_time = u'%02i:%02i' % (now.hour, now.minute)
        now_day = unicode(now.weekday())

        if now_time == u'00:00':
            self.is_locked = False

        # check lock
        lock = None
        if now_date in self.settings[u'lc_time_yearly'] and now_time in self.settings[u'lc_time_yearly'][now_date]:
            lock = True
        elif now_day in self.settings[u'lc_time_weekly'] and now_time in self.settings[u'lc_time_weekly'][now_day]:
            lock = True
        if now_date in self.settings[u'ul_time_yearly'] and now_time in self.settings[u'ul_time_yearly'][now_date]:
            lock = False
        elif now_day in self.settings[u'ul_time_weekly'] and now_time in self.settings[u'ul_time_weekly'][now_day]:
            lock = False

        if lock is not None:
            self.is_locked = lock

            # time.sleep(30)

    def set_run_day(self):
        self.run_day = True
