# -*- coding: utf-8 -*-

import datetime
import random
import os
import sqlite3
import hashlib
import re

import EncrConst
import MDConst


class SessionHandler(object):
    def __init__(self, master):
        self.maxId = 0
        self.master = master
        self.l = master.l
        self.mh = master.mh
        self.pws = EncryptedPasswordSave(MDConst.SETTINGSPATH + u'/udb', self)
        self.__sessions = dict()
        self.sessionTimeOut = datetime.timedelta(seconds=MDConst.LOGON_TIMEOUT)

    def __unicode__(self):
        return u'; '.join([unicode(i) for i in self.__sessions.values()])

    def getSessionById(self, session_id):
        try:
            return self.__sessions[session_id]
        except KeyError:
            return None

    def logOn(self, name, password):
        check, rights = self.pws.checkUserPassword(name, password)
        if check:
            session_id = self.__getNewSessionId()
            self.__sessions[session_id] = Session(session_id=session_id, name=name, rights=rights)
            self.l.logSession(u'Logon: name: %s' % name, logLevel=MDConst.LOG_INFO)
            self.mh.addMessage(u'%s hat sich eingeloggt' % name, level=MDConst.MESSAGE_INFO)
            self.maxId += 1
            return True, u'Benutzer wurde eingeloggt', session_id
        else:
            self.l.logSession(u'Fehlgeschlagener Anmedeversuch. Name: %s' % name,
                              logLevel=MDConst.LOG_ERROR)
            self.mh.addMessage(u'Fehlgeschlagener Anmedeversuch.', level=MDConst.MESSAGE_ERROR)
            return False, rights, ''  # in this case rights is a message

    def logOffById(self, session_id):
        session = self.getSessionById(session_id)
        if session:
            self.mh.addMessage(u'%s hat sich ausgeloggt' % session.name, level=MDConst.MESSAGE_INFO)
            self.logOff(session)

    def logOff(self, session):
        self.l.logSession(u'Logout: name: %s' % session.name, logLevel=MDConst.LOG_INFO)
        self.__sessions.pop(session.session_id, None)

    def checkRight(self, session_id, right):
        session = self.getSessionById(session_id)
        if not session:
            return False, u'Kein Benutzer ist eingeloggt'
        else:
            if right >= len(session.rights):
                return False, u'Das angeforderte Recht existiert nicht'
            elif session.rights[right] == '1':
                session.action()
                return True, u'Die Aktion darf ausgeführt werden'
            else:
                return False, u'Der Benutzer besitzt nicht das angeforderte Recht'

    def checkAnyLogon(self):
        return bool(self.__sessions)

    def checkOwnSession(self, session_id, name):
        session = self.getSessionById(session_id)
        if session:
            return session.name == name
        return False

    def update(self):
        for session in self.__sessions.values():
            if datetime.datetime.now() - session.lastActionTime > self.sessionTimeOut:
                self.mh.addMessage(u'%s wurde wegen Zeitüberschreitung ausgeloggt.' % session.name,
                                   level=MDConst.MESSAGE_INFO)
                self.logOff(session)
        self.master.master.i.write_info(u'Sessions', unicode(self))

    def __getNewSessionId(self):
        letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
        session_id = ''
        while not session_id or session_id in self.__sessions:
            session_id = ''
            for i in range(200):
                session_id += random.choice(letters)
        return session_id


class Session(object):
    def __init__(self, session_id, name, rights):
        self.session_id = session_id
        self.name = name
        self.rights = rights
        self.logOnTime = datetime.datetime.now()
        self.lastActionTime = datetime.datetime.now()

    def __str__(self):
        return 'Session: name: %s' % self.name

    def __unicode__(self):
        return u'Session: name: %s' % self.name

    def action(self):
        self.lastActionTime = datetime.datetime.now()


class EncryptedPasswordSave(object):
    def __init__(self, db_path, session_handler):
        self.db_path = db_path
        self.sh = session_handler
        if not os.path.isfile(self.db_path):
            try:
                self.create_epws()
            except sqlite3.Error as error:
                self.sh.l.logSession(u'Error occurred during creation of epws: %s' % str(error),
                                     logLevel=MDConst.LOG_ERROR)

    def create_epws(self):
        conn = sqlite3.connect(self.db_path)
        conn.execute('CREATE TABLE USERS('
                     'NAME TEXT NOT NULL UNIQUE,'
                     'PASSWORD TEXT NOT NULL,'
                     'SALT TEXT NOT NULL,'
                     'RIGHTS INT NOT NULL);')
        conn.execute("INSERT INTO USERS (NAME,PASSWORD,SALT,RIGHTS) VALUES ('admin', "
                     "'04c53d6454233045c5b83d59b426fb334b1d965ebabef78ee0fbd31147ed3183', "
                     "'wYcXi82yZajOeGSDMdIS', 65535);")
        conn.commit()
        conn.close()

    def __getUsersDataFromDb(self):
        conn = sqlite3.connect(self.db_path)
        cursor = conn.execute("SELECT NAME, PASSWORD, SALT, RIGHTS from USERS;")
        users = cursor.fetchall()
        cursor.close()
        users_data = {}
        for user in users:
            name, password, salt, rights_int = user
            temp = bin(rights_int)
            temp = temp[2:]
            temp = '%16s' % temp
            rights = temp.replace(' ', '0')
            users_data[name] = rights
        return users_data

    def checkUserPassword(self, name, password):
        try:
            conn = sqlite3.connect(self.db_path)
            cursor = conn.execute("SELECT NAME, PASSWORD, SALT, RIGHTS from USERS")
            users = cursor.fetchall()
            cursor.close()
        except sqlite3.Error as error:
            self.sh.l.logSession(u'Error occurred during access to epws: %s' % str(error), logLevel=MDConst.LOG_ERROR)
            return False, u'Datenbankfehler'
        for user_name, user_password, user_salt, user_rights in users:
            if user_name == name:
                encr_pw = hashlib.sha256(password + user_salt).hexdigest()
                if user_password == encr_pw:
                    temp = bin(user_rights)
                    temp = temp[2:]
                    temp = '%16s' % temp
                    rights = temp.replace(' ', '0')
                    return True, rights
                else:
                    return False, "Falsches Passwort"
        return False, "Unbekannter Benutzer"

    def getUsersData(self, session_id):
        view_right = self.sh.checkRight(session_id, EncrConst.VIEW)
        root_right = self.sh.checkRight(session_id, EncrConst.ROOT)
        if not view_right[0] and not root_right[0]:
            return view_right
        try:
            users_data = self.__getUsersDataFromDb()
        except sqlite3.Error as error:
            self.sh.l.logSession(u'Error occurred during access to epws: %s' % str(error), logLevel=MDConst.LOG_ERROR)
            return False, u'Datenbankfehler'
        if not root_right[0]:
            for user in users_data:
                users_data[user] = ''
        return True, users_data

    @staticmethod
    def __checkPasswordFitness(password):
        if not isinstance(password, (unicode,)):
            return False, u'Passwort muss vom Typ unicode sein.'
        if len(password) < 8:
            return False, u'Passwort ist zu kurz. Es muss mindestens 8 Zeichen haben.'
        if len(password) > 20:
            return False, u'Passwort ist zu Lang. Es darf maximal 20 Zeichen lang sein.'
        if not re.match(r'\A[a-zA-Z0-9]+\Z', password):
            return False, u'Es befinden sich unzulässige Zeichen im Passwort'
        return True, u'Passwort ist in Ordnung.'

    def __checkNameFitness(self, name):
        if not isinstance(name, (unicode,)):
            return False, u'Benutzername muss unicode oder str sein'
        if len(name) < 4:
            return False, u'Benutzername muss mindestens 4 Zeichen haben'
        if len(name) > 20:
            return False, u'Benutzername darf maximal 20 Zeichen lang haben'
        if not re.match(r'\A[a-zA-Z]+[a-zA-Z 0-9]*[a-zA-Z0-9]+\Z', name):
            return False, u'Aufbau des Namens ist unzulässig'
        try:
            users_data = self.__getUsersDataFromDb()
        except sqlite3.Error as error:
            self.sh.l.logSession(u'Error occurred during access to epws: %s' % str(error), logLevel=MDConst.LOG_ERROR)
            return False, u'Datenbankfehler'
        if name in users_data:
            return False, u'Name bereits vorhanden'
        return True, u'Name ist in Ordnung'

    def addUser(self, session_id, new_name, new_password1, new_password2, new_rights):
        right = self.sh.checkRight(session_id, EncrConst.ROOT)
        if not right[0]:
            return right
        name_check = self.__checkNameFitness(new_name)
        if not name_check[0]:
            return name_check
        pw_check = self.__checkNameFitness(new_password1)
        if not pw_check[0]:
            return pw_check
        if not new_password1 == new_password2:
            return False, u'Passwörter stimmen nicht überein'

        if not isinstance(new_rights, (str, unicode)):
            return False, u'Internal Error'
        if not len(new_rights) == 16:
            return False, u'Internal Error'
        for right in new_rights:
            if right not in ('0', '1', u'0', u'1'):
                return False, u'Internal Error'

        salt = self.__get_salt()
        encr_pw = hashlib.sha256(new_password1 + salt).hexdigest()

        try:
            conn = sqlite3.connect(self.db_path)
            conn.execute("INSERT INTO USERS (NAME, PASSWORD, SALT, RIGHTS) VALUES (?,?,?,?);",
                         [new_name, encr_pw, salt, int(new_rights, 2)])
            conn.commit()
            conn.close()
        except sqlite3.Error as error:
            self.sh.l.logSession(u'Error occurred during access to epws: %s' % str(error), logLevel=MDConst.LOG_ERROR)
            return False, u'Datenbankfehler'
        return True, u'Neuen Benutzer hinzugefügt'

    def changePw(self, session_id, name, new_password1, new_password2):
        right = self.sh.checkRight(session_id, EncrConst.ROOT)
        if not right[0] and not self.sh.checkOwnSession(session_id, name):
            return right

        pw_check = self.__checkPasswordFitness(new_password1)
        if not pw_check[0]:
            return pw_check
        if not new_password1 == new_password2:
            return False, u'Passwörter stimmen nicht überein'

        salt = self.__get_salt()
        encr_pw = hashlib.sha256(new_password1 + salt).hexdigest()

        try:
            conn = sqlite3.connect(self.db_path)
            conn.execute("UPDATE USERS SET PASSWORD = ?, SALT = ? WHERE NAME = ?;", [encr_pw, salt, name])
            conn.commit()
            conn.close()
        except sqlite3.Error as error:
            self.sh.l.logSession(u'Error occurred during access to epws: %s' % str(error), logLevel=MDConst.LOG_ERROR)
            return False, u'Datenbankfehler'
        return True, u'Passwort geändert'

    def setUserRights(self, session_id, name, new_rights):
        right = self.sh.checkRight(session_id, EncrConst.ROOT)
        if not right[0]:
            return right

        try:
            users_data = self.__getUsersDataFromDb()
        except sqlite3.Error as error:
            self.sh.l.logSession(u'Error occurred during access to epws: %s' % str(error), logLevel=MDConst.LOG_ERROR)
            return False, u'Datenbankfehler'

        if name not in users_data:
            return False, u'Unbekannter Benutzer'

        # to prevent admin to change his root right
        session = self.sh.getSessionById(session_id)
        if session.name == name:
            new_rights = users_data[name][0] + new_rights[1:]

        if not isinstance(new_rights, (str, unicode)):
            return False, u'Internal Error'
        if not len(new_rights) == 16:
            return False, u'Internal Error'
        for i in new_rights:
            if i not in ('0', '1', u'0', u'1'):
                return False, u'Internal Error'

        try:
            conn = sqlite3.connect(self.db_path)
            conn.execute("UPDATE USERS SET RIGHTS = ? WHERE NAME = ?;", [int(new_rights, 2), name])
            conn.commit()
            conn.close()
        except sqlite3.Error as error:
            self.sh.l.logSession(u'Error occurred during access to epws: %s' % str(error), logLevel=MDConst.LOG_ERROR)
            return False, u'Datenbankfehler'
        return True, u'Nutzerrechte geändert'

    def deleteUser(self, session_id, name):
        right = self.sh.checkRight(session_id, EncrConst.ROOT)
        if not right[0]:
            return right
        if self.sh.checkOwnSession(session_id, name):
            return False, u'Du versuchst nicht ernsthaft dich selbst zu löschen?'

        try:
            conn = sqlite3.connect(self.db_path)
            conn.execute("DELETE FROM USERS WHERE NAME = ?;", [name])
            conn.commit()
            conn.close()
        except sqlite3.Error as error:
            self.sh.l.logSession(u'Error occurred during access to epws: %s' % str(error), logLevel=MDConst.LOG_ERROR)
            return False, u'Datenbankfehler'
        return True, u'Nutzerkonto gelöscht'

    @staticmethod
    def __get_salt():
        letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
        salt = ''
        for i in range(20):
            salt += random.choice(letters)
        return salt
