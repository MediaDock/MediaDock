import platform

if platform.system() == 'Windows':
    from OsInterfaceW7_8_10 import *
elif platform.system() == 'Linux':
    from OsInterfaceLinux import *
    import fcntl